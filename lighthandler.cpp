#include "lighthandler.h"

LightHandler::LightHandler(QTreeWidget* ls)
{
    list=ls;
}

int LightHandler::addLight(){
    glEnable(GL_LIGHT0+(lights.size()));
    lightStruct l = {{{0,0,0},{0,0,0},{0,0,0}}};
    lights.push_back(l);
    glLightfv(GL_LIGHT0+(lights.size()-1), GL_AMBIENT, l.l[0]);
    glLightfv(GL_LIGHT0+(lights.size()-1), GL_DIFFUSE, l.l[1]);
    glLightfv(GL_LIGHT0+(lights.size()-1), GL_SPECULAR, l.l[2]);
    return lights.size()-1;
}
int LightHandler::addProjectLight(GLfloat val1, GLfloat val2, GLfloat val3, GLfloat val4, GLfloat val5, GLfloat val6, GLfloat val7, GLfloat val8, GLfloat val9){
    glEnable(GL_LIGHT0+(lights.size()));
    lightStruct l = {{{val1,val2,val3},{val4,val5,val6},{val7,val8,val9}}};
    lights.push_back(l);
    glLightfv(GL_LIGHT0+(lights.size()-1), GL_AMBIENT, l.l[0]);
    glLightfv(GL_LIGHT0+(lights.size()-1), GL_DIFFUSE, l.l[1]);
    glLightfv(GL_LIGHT0+(lights.size()-1), GL_SPECULAR, l.l[2]);
    return lights.size()-1;
}
void LightHandler::removeLight(int id){
    lights.erase(lights.begin()+id);
    glDisable(GL_LIGHT0+(lights.size()));
    for(int i=0;i<list->topLevelItemCount();i++){
        ObjectItem* item = static_cast<ObjectItem *>(list->topLevelItem(i));
        if(item->data.lightID>-1){
            if(item->data.lightID>id){
                item->data.lightID-=1;
            }
            glLightfv(GL_LIGHT0+(item->data.lightID), GL_AMBIENT, lights.at(item->data.lightID).l[0]);
            glLightfv(GL_LIGHT0+(item->data.lightID), GL_DIFFUSE, lights.at(item->data.lightID).l[1]);
            glLightfv(GL_LIGHT0+(item->data.lightID), GL_SPECULAR, lights.at(item->data.lightID).l[2]);
        }
    }
}
void LightHandler::updateLight(int id, int prop, float value){
    GLfloat realvalue = value/100;
    if(prop==1){
        lights.at(id).l[0][0]=realvalue;
        glLightfv(GL_LIGHT0+id, GL_AMBIENT, lights.at(id).l[0]);
    } else if(prop==2){
        lights.at(id).l[0][1]=realvalue;
        glLightfv(GL_LIGHT0+id, GL_AMBIENT, lights.at(id).l[0]);
    } else if(prop==3){
        lights.at(id).l[0][2]=realvalue;
        glLightfv(GL_LIGHT0+id, GL_AMBIENT, lights.at(id).l[0]);
    } else if(prop==4){
        lights.at(id).l[1][0]=realvalue;
        glLightfv(GL_LIGHT0+id, GL_DIFFUSE, lights.at(id).l[1]);
    } else if(prop==5){
        lights.at(id).l[1][1]=realvalue;
        glLightfv(GL_LIGHT0+id, GL_DIFFUSE, lights.at(id).l[1]);
    } else if(prop==6){
        lights.at(id).l[1][2]=realvalue;
        glLightfv(GL_LIGHT0+id, GL_DIFFUSE, lights.at(id).l[1]);
    } else if(prop==7){
        lights.at(id).l[2][0]=realvalue;
        glLightfv(GL_LIGHT0+id, GL_SPECULAR, lights.at(id).l[2]);
    } else if(prop==8){
        lights.at(id).l[2][1]=realvalue;
        glLightfv(GL_LIGHT0+id, GL_SPECULAR, lights.at(id).l[2]);
    } else if(prop==9){
        lights.at(id).l[2][2]=realvalue;
        glLightfv(GL_LIGHT0+id, GL_SPECULAR, lights.at(id).l[2]);
    }
}
