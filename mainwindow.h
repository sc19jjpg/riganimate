#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGLWidget>
#include <QBoxLayout>
#include <renderarea.h>
#include <modelhandler.h>
#include <objectItem.h>
#include <boneitem.h>
#include <timeline.h>
#include <lighthandler.h>
#include <QTreeWidget>
#include <QDebug>
#include <QLabel>
#include <QString>
#include <QLineEdit>
#include <QComboBox>
#include <QGraphicsSceneMouseEvent>
#include <QDoubleSpinBox>
#include <QPushButton>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QTreeView>
#include <QStandardItem>
#include <iostream>
#include <QFileDialog>
#include <QLabel>
#include <QComboBox>
#include <QSpinBox>
#include <projectinfo.h>
#include <projecthandler.h>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    void setObjectSettings(ObjectItem * item);
    void setBoneSettings(BoneItem * item);
    void updateRotationValues();
    ~MainWindow();

    projectInfoStruct projectInfo;
    QVBoxLayout* objectSettings;
    QLabel* objectName;
    QLineEdit* objectNameSetting;
    QTabWidget* objectPositionSettings;
    QLabel* boneLengthLabel;
    QDoubleSpinBox* boneLengthSetting;
    QComboBox *objectType;
    QComboBox *objectModel;
    RenderArea *renderArea;
    ModelHandler* modelHandler;
    Timeline *timeline;
    LightHandler *lightHandler;
    QTreeWidget *list;
    QDoubleSpinBox* xPosNum;
    QDoubleSpinBox* yPosNum;
    QDoubleSpinBox* zPosNum;
    QDoubleSpinBox* xRotNum;
    QDoubleSpinBox* yRotNum;
    QDoubleSpinBox* zRotNum;
    QDoubleSpinBox* wRotNum;
    QDoubleSpinBox* xScaleNum;
    QDoubleSpinBox* yScaleNum;
    QDoubleSpinBox* zScaleNum;
    QPushButton* textureBtn;
    QPushButton* lightBtn;
    ProjectHandler* projectHandler;

public slots:
    void selectObject();
    void saveProject();
    void importProject();
    void addObject();
    void removeObject();
    void toggleMode();
    void addBone();
    void togglePlay();
    void addKeyframe();
    void removeKeyframe();
    void updateName(QString);
    void updateTexture();
    void setBoneLength(double);
    void updateObjectType(int);
    void updateObjectModel(int);
    void openSettings();
    void updateFrameNumber(int);
    void updateFrameRate(int);
    void updateObjectXPos(double);
    void updateObjectYPos(double);
    void updateObjectZPos(double);
    void updateObjectXRot(double);
    void updateObjectYRot(double);
    void updateObjectZRot(double);
    void updateObjectWRot(double);
    void updateObjectXScale(double);
    void updateObjectYScale(double);
    void updateObjectZScale(double);
    void lightDialog();
    void updateLightAmbientX(int);
    void updateLightAmbientY(int);
    void updateLightAmbientZ(int);
    void updateLightDiffuseX(int);
    void updateLightDiffuseY(int);
    void updateLightDiffuseZ(int);
    void updateLightSpecularX(int);
    void updateLightSpecularY(int);
    void updateLightSpecularZ(int);
};
#endif // MAINWINDOW_H
