#ifndef OBJECTITEM_H
#define OBJECTITEM_H

#include <QTreeWidgetItem>
#include <keyframe.h>
#include <quaternion.h>
#include <QImage>
#include <vector>

typedef struct sceneObjectStruct {
    int type;
    int modelId;
    double pos[3];
    double scale[3];
    Quaternion* q;
    std::vector<KeyframeStruct> keyframes;
    std::string texturePath;
    QImage texture;
    int lightID;
} sceneObjectStruct;

class ObjectItem : public QTreeWidgetItem
{
public:
    ObjectItem();
    ObjectItem(sceneObjectStruct object);
    //virtual ~ObjectItem();
    int addKeyframe(int frame);
    void addProjectKeyframe(KeyframeStruct k);
    void removeKeyframe(int frame);
    void deleteExtraKeyframes(int newFrameNum);
    sceneObjectStruct data;
    float* generateMatrix();
};

#endif // OBJECTITEM_H
