#include "timeline.h"

Timeline::Timeline(projectInfoStruct* pi, QTreeWidget* ls)
{
    projectInfo=pi;
    list=ls;
    QGraphicsScene();
    setSceneRect(0,0, projectInfo->frameNumber, 100);
    cursor = addLine(QLine(0,0,0,100),QPen(QColor(51,102,255),3));
}
void Timeline::mouseReleaseEvent(QGraphicsSceneMouseEvent *e){
    int frame = e->scenePos().x();
    if(frame<0){
        projectInfo->currentFrame=0;
    } else if(frame>projectInfo->frameNumber){
        projectInfo->currentFrame=projectInfo->frameNumber;
    } else {
        projectInfo->currentFrame=frame;
    }
    setFrame(projectInfo->currentFrame);
}

void Timeline::setFrame(int frame){
    cursor->setX(projectInfo->currentFrame);
    for(int i=0;i<list->topLevelItemCount();i++){
        ObjectItem* so = static_cast<ObjectItem *>(list->topLevelItem(i));
        if(so->data.type==0) continue; //type not defined yet
        if(so->data.type==1 || so->data.type==3){ //simple object or ligth
            if(so->data.keyframes.size()==0){
                continue;
            }
            if(so->data.keyframes.size()==1){
                setObjectPose(so,so->data.keyframes.at(0));
                continue;
            }
            KeyframeStruct previousKeyframe;
            KeyframeStruct nextKeyframe;
            bool previousNull=true;
            bool nextNull=true;
            for(int j=0;j<(int)so->data.keyframes.size();j++){
                if(so->data.keyframes.at(j).frame==frame){
                    previousKeyframe=so->data.keyframes.at(j);
                    nextKeyframe=so->data.keyframes.at(j);
                    previousNull=false;
                    nextNull=false;
                    break;
                }
                if(so->data.keyframes.at(j).frame<frame && (previousNull || so->data.keyframes.at(j).frame>previousKeyframe.frame)){
                    previousKeyframe=so->data.keyframes.at(j);
                    previousNull=false;
                }
                if(so->data.keyframes.at(j).frame>frame && (nextNull || so->data.keyframes.at(j).frame<nextKeyframe.frame)){
                    nextKeyframe=so->data.keyframes.at(j);
                    nextNull=false;
                }
            }
            if(previousNull && nextNull){
                continue;
            }
            if(previousNull){
                setObjectPose(so,nextKeyframe);
                continue;
            }
            if(nextNull){
                setObjectPose(so,previousKeyframe);
                continue;
            }
            if(previousKeyframe.frame == nextKeyframe.frame){
                setObjectPose(so,previousKeyframe);
                continue;
            }
            float interpolationFactor=(float)(frame-previousKeyframe.frame)/(float)(nextKeyframe.frame-previousKeyframe.frame);
            setObjectInterpolatedPose(so,previousKeyframe,nextKeyframe,interpolationFactor);
            continue;
        }
        if(so->data.type==2){
            for(int j=so->childCount()-1; j>=0;j--){
                BoneItem* bone = static_cast<BoneItem *>(so->child(j));
                setBoneFrame(bone, frame);
            }
            continue;
        }
    }
}
void Timeline::setBoneFrame(BoneItem *so,int frame){
    if(so->data.keyframes.size()==0){
        //no keyframes
    } else if(so->data.keyframes.size()==1){
        setBonePose(so,so->data.keyframes.at(0));
    } else {
        KeyframeStruct previousKeyframe;
        KeyframeStruct nextKeyframe;
        bool previousNull=true;
        bool nextNull=true;
        for(int j=0;j<(int)so->data.keyframes.size();j++){
            if(so->data.keyframes.at(j).frame==frame){
                previousKeyframe=so->data.keyframes.at(j);
                nextKeyframe=so->data.keyframes.at(j);
                previousNull=false;
                nextNull=false;
                break;
            }
            if(so->data.keyframes.at(j).frame<frame && (previousNull || so->data.keyframes.at(j).frame>previousKeyframe.frame)){
                previousKeyframe=so->data.keyframes.at(j);
                previousNull=false;
            }
            if(so->data.keyframes.at(j).frame>frame && (nextNull || so->data.keyframes.at(j).frame<nextKeyframe.frame)){
                nextKeyframe=so->data.keyframes.at(j);
                nextNull=false;
            }
        }
        if(previousNull && nextNull){
            //no keyframes found
        } else if(previousNull){
            setBonePose(so,nextKeyframe);
            //only next keyframe found
        } else if(nextNull){
            setBonePose(so,previousKeyframe);
            //only previous keyframe found
        } else if(previousKeyframe.frame == nextKeyframe.frame){
            setBonePose(so,previousKeyframe);
            //previous and next are the same
        } else {
            float interpolationFactor=(float)(frame-previousKeyframe.frame)/(float)(nextKeyframe.frame-previousKeyframe.frame);
            setBoneInterpolatedPose(so,previousKeyframe,nextKeyframe,interpolationFactor);
        }
    }
    for(int j=so->childCount()-1; j>=0;j--){
        BoneItem* bone = static_cast<BoneItem *>(so->child(j));
        setBoneFrame(bone, frame);
    }
}
void Timeline::setObjectPose(ObjectItem* so, KeyframeStruct keyframe){
    so->data.pos[0]=keyframe.pos[0];
    so->data.pos[1]=keyframe.pos[1];
    so->data.pos[2]=keyframe.pos[2];
    so->data.q->x=keyframe.q->x;
    so->data.q->y=keyframe.q->y;
    so->data.q->z=keyframe.q->z;
    so->data.q->w=keyframe.q->w;
    so->data.scale[0]=keyframe.scale[0];
    so->data.scale[1]=keyframe.scale[1];
    so->data.scale[2]=keyframe.scale[2];
}
void Timeline::setBonePose(BoneItem* so, KeyframeStruct keyframe){
    so->data.pos[0]=keyframe.pos[0];
    so->data.pos[1]=keyframe.pos[1];
    so->data.pos[2]=keyframe.pos[2];
    so->data.q->x=keyframe.q->x;
    so->data.q->y=keyframe.q->y;
    so->data.q->z=keyframe.q->z;
    so->data.q->w=keyframe.q->w;
    so->data.scale[0]=keyframe.scale[0];
    so->data.scale[1]=keyframe.scale[1];
    so->data.scale[2]=keyframe.scale[2];
}
void Timeline::setObjectInterpolatedPose(ObjectItem* so, KeyframeStruct previousKeyframe, KeyframeStruct nextKeyframe, float interpolationFactor){
    so->data.pos[0]=previousKeyframe.pos[0]*(1-interpolationFactor)+nextKeyframe.pos[0]*interpolationFactor;
    so->data.pos[1]=previousKeyframe.pos[1]*(1-interpolationFactor)+nextKeyframe.pos[1]*interpolationFactor;
    so->data.pos[2]=previousKeyframe.pos[2]*(1-interpolationFactor)+nextKeyframe.pos[2]*interpolationFactor;
    so->data.q->x=previousKeyframe.q->x*(1-interpolationFactor)+nextKeyframe.q->x*interpolationFactor;
    so->data.q->y=previousKeyframe.q->y*(1-interpolationFactor)+nextKeyframe.q->y*interpolationFactor;
    so->data.q->z=previousKeyframe.q->z*(1-interpolationFactor)+nextKeyframe.q->z*interpolationFactor;
    so->data.q->w=previousKeyframe.q->w*(1-interpolationFactor)+nextKeyframe.q->w*interpolationFactor;
    so->data.q->normalize();
    so->data.scale[0]=previousKeyframe.scale[0]*(1-interpolationFactor)+nextKeyframe.scale[0]*interpolationFactor;
    so->data.scale[1]=previousKeyframe.scale[1]*(1-interpolationFactor)+nextKeyframe.scale[1]*interpolationFactor;
    so->data.scale[2]=previousKeyframe.scale[2]*(1-interpolationFactor)+nextKeyframe.scale[2]*interpolationFactor;
}
void Timeline::setBoneInterpolatedPose(BoneItem* so, KeyframeStruct previousKeyframe, KeyframeStruct nextKeyframe, float interpolationFactor){
    so->data.pos[0]=previousKeyframe.pos[0]*(1-interpolationFactor)+nextKeyframe.pos[0]*interpolationFactor;
    so->data.pos[1]=previousKeyframe.pos[1]*(1-interpolationFactor)+nextKeyframe.pos[1]*interpolationFactor;
    so->data.pos[2]=previousKeyframe.pos[2]*(1-interpolationFactor)+nextKeyframe.pos[2]*interpolationFactor;
    so->data.q->x=previousKeyframe.q->x*(1-interpolationFactor)+nextKeyframe.q->x*interpolationFactor;
    so->data.q->y=previousKeyframe.q->y*(1-interpolationFactor)+nextKeyframe.q->y*interpolationFactor;
    so->data.q->z=previousKeyframe.q->z*(1-interpolationFactor)+nextKeyframe.q->z*interpolationFactor;
    so->data.q->w=previousKeyframe.q->w*(1-interpolationFactor)+nextKeyframe.q->w*interpolationFactor;
    so->data.q->normalize();
    so->data.scale[0]=previousKeyframe.scale[0]*(1-interpolationFactor)+nextKeyframe.scale[0]*interpolationFactor;
    so->data.scale[1]=previousKeyframe.scale[1]*(1-interpolationFactor)+nextKeyframe.scale[1]*interpolationFactor;
    so->data.scale[2]=previousKeyframe.scale[2]*(1-interpolationFactor)+nextKeyframe.scale[2]*interpolationFactor;
}
