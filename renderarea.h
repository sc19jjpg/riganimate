#ifndef RENDERAREA_H
#define RENDERAREA_H

#include <QGLWidget>
#include <modelhandler.h>
#include <objectItem.h>
#include <boneitem.h>
#include <QTreeWidget>
#include <QMouseEvent>
#include <QTimer>
#include <QElapsedTimer>
#include <QDoubleSpinBox>
#include <timeline.h>
#include <projectinfo.h>

class RenderArea: public QGLWidget
{
    Q_OBJECT
public:
    RenderArea(QWidget *parent, QTreeWidget* ls,ModelHandler* mh, projectInfoStruct* pi, Timeline* tl, QDoubleSpinBox* xp, QDoubleSpinBox* yp, QDoubleSpinBox* zp, QDoubleSpinBox* xr, QDoubleSpinBox* yr, QDoubleSpinBox* zr, QDoubleSpinBox* wr, QDoubleSpinBox* xs, QDoubleSpinBox* ys, QDoubleSpinBox* zs);
    void renderObjects();
    void renderRig(BoneItem* bone);
    void play();
    void pause();
    ModelHandler* modelHandler;
    QTreeWidget *list;
    projectInfoStruct* projectInfo;
    Timeline* timeline;
    float ndc_x, ndc_y, ndc_z;
    int width;
    int height;
    float radius;
    float press_x,press_y,press_z,press_w;
    QTimer* timer;
    QElapsedTimer* elapsed;
    QDoubleSpinBox* xPosNum;
    QDoubleSpinBox* yPosNum;
    QDoubleSpinBox* zPosNum;
    QDoubleSpinBox* xRotNum;
    QDoubleSpinBox* yRotNum;
    QDoubleSpinBox* zRotNum;
    QDoubleSpinBox* wRotNum;
    QDoubleSpinBox* xScaleNum;
    QDoubleSpinBox* yScaleNum;
    QDoubleSpinBox* zScaleNum;
protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();
public slots:
    void mousePressEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent* e);
    void timerUpdate();
};

#endif // RENDERAREA_H
