#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    projectInfo.currentFrame=0;
    projectInfo.frameNumber=60*10;
    projectInfo.frameRate=60;
    projectInfo.windowMode=false;
    projectInfo.isPlaying=false;
    QWidget *windowWidget = new QWidget;

    QHBoxLayout *windowLayout = new QHBoxLayout();

    QVBoxLayout *left = new QVBoxLayout();
    QVBoxLayout *right = new QVBoxLayout();

    QHBoxLayout *top = new QHBoxLayout();
    QHBoxLayout *render = new QHBoxLayout();
    QVBoxLayout *bottom = new QVBoxLayout();

    QWidget *bottomWidget = new QWidget;

    QHBoxLayout *bottomButtons = new QHBoxLayout();
    QHBoxLayout *timelineLayout = new QHBoxLayout();

    QPushButton *saveBtn = new QPushButton();
    saveBtn->setMinimumSize(QSize(50,50));
    saveBtn->setMaximumSize(QSize(50,50));
    saveBtn->setIcon(QIcon("icons/save.png"));
    saveBtn->setIconSize(QSize(50,50));
    connect(saveBtn, SIGNAL(clicked()), this, SLOT(saveProject()));

    QPushButton *importBtn = new QPushButton();
    importBtn->setMinimumSize(QSize(50,50));
    importBtn->setMaximumSize(QSize(50,50));
    importBtn->setIcon(QIcon("icons/import.png"));
    importBtn->setIconSize(QSize(50,50));
    connect(importBtn, SIGNAL(clicked()), this, SLOT(importProject()));

    QPushButton *addObjectBtn = new QPushButton();
    addObjectBtn->setMinimumSize(QSize(50,50));
    addObjectBtn->setMaximumSize(QSize(50,50));
    addObjectBtn->setIcon(QIcon("icons/add.png"));
    addObjectBtn->setIconSize(QSize(50,50));
    connect(addObjectBtn, SIGNAL(clicked()), this, SLOT(addObject()));

    QPushButton *removeObjectBtn = new QPushButton();
    removeObjectBtn->setMinimumSize(QSize(50,50));
    removeObjectBtn->setMaximumSize(QSize(50,50));
    removeObjectBtn->setIcon(QIcon("icons/delete.png"));
    removeObjectBtn->setIconSize(QSize(50,50));
    connect(removeObjectBtn, SIGNAL(clicked()), this, SLOT(removeObject()));

    QPushButton *toggleModeBtn = new QPushButton();
    toggleModeBtn->setMinimumSize(QSize(50,50));
    toggleModeBtn->setMaximumSize(QSize(50,50));
    toggleModeBtn->setIcon(QIcon("icons/toggle-rig.png"));
    toggleModeBtn->setIconSize(QSize(50,50));
    connect(toggleModeBtn, SIGNAL(clicked()), this, SLOT(toggleMode()));

    QPushButton *addBoneBtn = new QPushButton();
    addBoneBtn->setMinimumSize(QSize(50,50));
    addBoneBtn->setMaximumSize(QSize(50,50));
    addBoneBtn->setIcon(QIcon("icons/addbone.png"));
    addBoneBtn->setIconSize(QSize(50,50));
    connect(addBoneBtn, SIGNAL(clicked()), this, SLOT(addBone()));

    top->setAlignment(Qt::AlignLeft);

    QPushButton *playBtn = new QPushButton();
    playBtn->setMinimumSize(QSize(50,50));
    playBtn->setMaximumSize(QSize(50,50));
    playBtn->setIcon(QIcon("icons/play.png"));
    playBtn->setIconSize(QSize(50,50));
    connect(playBtn, SIGNAL(clicked()), this, SLOT(togglePlay()));

    QPushButton *addKeyframeBtn = new QPushButton();
    addKeyframeBtn->setMinimumSize(QSize(50,50));
    addKeyframeBtn->setMaximumSize(QSize(50,50));
    addKeyframeBtn->setIcon(QIcon("icons/add.png"));
    addKeyframeBtn->setIconSize(QSize(50,50));
    connect(addKeyframeBtn, SIGNAL(clicked()), this, SLOT(addKeyframe()));

    QPushButton *removeKeyframeBtn = new QPushButton();
    removeKeyframeBtn->setMinimumSize(QSize(50,50));
    removeKeyframeBtn->setMaximumSize(QSize(50,50));
    removeKeyframeBtn->setIcon(QIcon("icons/delete.png"));
    removeKeyframeBtn->setIconSize(QSize(50,50));
    connect(removeKeyframeBtn, SIGNAL(clicked()), this, SLOT(removeKeyframe()));

    QPushButton *settingsBtn = new QPushButton();
    settingsBtn->setMinimumSize(QSize(50,50));
    settingsBtn->setMaximumSize(QSize(50,50));
    settingsBtn->setIcon(QIcon("icons/settings.png"));
    settingsBtn->setIconSize(QSize(50,50));
    connect(settingsBtn, SIGNAL(clicked()), this, SLOT(openSettings()));

    top->addWidget(saveBtn);
    top->addWidget(importBtn);
    top->addWidget(addObjectBtn);
    top->addWidget(removeObjectBtn);
    top->addWidget(toggleModeBtn);
    top->addWidget(addBoneBtn);

    bottomButtons->setAlignment(Qt::AlignCenter);

    bottomButtons->addWidget(playBtn);
    bottomButtons->addWidget(addKeyframeBtn);
    bottomButtons->addWidget(removeKeyframeBtn);
    bottomButtons->addWidget(settingsBtn);

    bottomWidget->setLayout(bottom);

    bottom->addLayout(bottomButtons);
    bottom->addLayout(timelineLayout);

    objectType = new QComboBox();
    objectType->addItem("Select Type");
    objectType->addItem("Object");
    objectType->addItem("Rig");
    objectType->addItem("Light");
    connect(objectType, SIGNAL(currentIndexChanged(int)), this, SLOT(updateObjectType(int)));

    objectSettings = new QVBoxLayout();

    objectName= new QLabel();
    objectNameSetting = new QLineEdit();

    connect(objectNameSetting, SIGNAL(textChanged(QString)), this, SLOT(updateName(QString)));

    objectModel = new QComboBox(this);
    objectModel->addItem("Select Model");
    objectModel->addItem("New Model");
    connect(objectModel, SIGNAL(currentIndexChanged(int)), this, SLOT(updateObjectModel(int)));

    boneLengthLabel = new QLabel("Bone Lenght");
    boneLengthSetting = new QDoubleSpinBox();
    connect(boneLengthSetting, SIGNAL(valueChanged(double)), this, SLOT(setBoneLength(double)));

    objectPositionSettings = new QTabWidget();

    QHBoxLayout* xPosLayout = new QHBoxLayout();
    QLabel* xPosLabel = new QLabel("X");
    xPosNum = new QDoubleSpinBox;
    xPosNum->setRange(-200,200);
    connect(xPosNum, SIGNAL(valueChanged(double)), this, SLOT(updateObjectXPos(double)));

    QHBoxLayout* yPosLayout = new QHBoxLayout();
    QLabel* yPosLabel = new QLabel("Y");
    yPosNum = new QDoubleSpinBox;
    yPosNum->setRange(-200,200);
    connect(yPosNum, SIGNAL(valueChanged(double)), this, SLOT(updateObjectYPos(double)));

    QHBoxLayout* zPosLayout = new QHBoxLayout();
    QLabel* zPosLabel = new QLabel("Z");
    zPosNum = new QDoubleSpinBox;
    zPosNum->setRange(-200,200);
    connect(zPosNum, SIGNAL(valueChanged(double)), this, SLOT(updateObjectZPos(double)));

    xPosLayout->addWidget(xPosLabel,0);
    xPosLayout->addWidget(xPosNum,1);

    yPosLayout->addWidget(yPosLabel,0);
    yPosLayout->addWidget(yPosNum,1);

    zPosLayout->addWidget(zPosLabel,0);
    zPosLayout->addWidget(zPosNum,1);

    QWidget* posWidget = new QWidget();

    QVBoxLayout* posLayout = new QVBoxLayout();
    posLayout->addLayout(xPosLayout);
    posLayout->addLayout(yPosLayout);
    posLayout->addLayout(zPosLayout);

    posWidget->setLayout(posLayout);

    objectPositionSettings->addTab(posWidget,QIcon("icons/pos.png"),"Position");

    //rotation

    QHBoxLayout* xRotLayout = new QHBoxLayout();
    QLabel* xRotLabel = new QLabel("X");
    xRotNum = new QDoubleSpinBox;
    connect(xRotNum, SIGNAL(valueChanged(double)), this, SLOT(updateObjectXRot(double)));
    xRotNum->setRange(-1,1);
    xRotNum->setSingleStep(0.001);

    QHBoxLayout* yRotLayout = new QHBoxLayout();
    QLabel* yRotLabel = new QLabel("Y");
    yRotNum = new QDoubleSpinBox;
    yRotNum->setRange(-360,360);
    connect(yRotNum, SIGNAL(valueChanged(double)), this, SLOT(updateObjectYRot(double)));
    yRotNum->setRange(-1,1);
    yRotNum->setSingleStep(0.001);

    QHBoxLayout* zRotLayout = new QHBoxLayout();
    QLabel* zRotLabel = new QLabel("Z");
    zRotNum = new QDoubleSpinBox;
    connect(zRotNum, SIGNAL(valueChanged(double)), this, SLOT(updateObjectZRot(double)));
    zRotNum->setRange(-1,1);
    zRotNum->setSingleStep(0.001);

    QHBoxLayout* wRotLayout = new QHBoxLayout();
    QLabel* wRotLabel = new QLabel("W");
    wRotNum = new QDoubleSpinBox;
    connect(wRotNum, SIGNAL(valueChanged(double)), this, SLOT(updateObjectWRot(double)));
    wRotNum->setRange(-1,1);
    wRotNum->setSingleStep(0.001);

    xRotLayout->addWidget(xRotLabel,0);
    xRotLayout->addWidget(xRotNum,1);

    yRotLayout->addWidget(yRotLabel,0);
    yRotLayout->addWidget(yRotNum,1);

    zRotLayout->addWidget(zRotLabel,0);
    zRotLayout->addWidget(zRotNum,1);

    wRotLayout->addWidget(wRotLabel,0);
    wRotLayout->addWidget(wRotNum,1);

    QWidget* rotWidget = new QWidget();

    QVBoxLayout* rotLayout = new QVBoxLayout();
    rotLayout->addLayout(xRotLayout);
    rotLayout->addLayout(yRotLayout);
    rotLayout->addLayout(zRotLayout);
    rotLayout->addLayout(wRotLayout);

    rotWidget->setLayout(rotLayout);

    objectPositionSettings->addTab(rotWidget,QIcon("icons/rot.png"),"Rotation");

    //scale

    QHBoxLayout* xScaleLayout = new QHBoxLayout();
    QLabel* xScaleLabel = new QLabel("X");
    xScaleNum = new QDoubleSpinBox;
    connect(xScaleNum, SIGNAL(valueChanged(double)), this, SLOT(updateObjectXScale(double)));

    QHBoxLayout* yScaleLayout = new QHBoxLayout();
    QLabel* yScaleLabel = new QLabel("Y");
    yScaleNum = new QDoubleSpinBox;
    connect(yScaleNum, SIGNAL(valueChanged(double)), this, SLOT(updateObjectYScale(double)));

    QHBoxLayout* zScaleLayout = new QHBoxLayout();
    QLabel* zScaleLabel = new QLabel("Z");
    zScaleNum = new QDoubleSpinBox;
    connect(zScaleNum, SIGNAL(valueChanged(double)), this, SLOT(updateObjectZScale(double)));

    xScaleLayout->addWidget(xScaleLabel,0);
    xScaleLayout->addWidget(xScaleNum,1);

    yScaleLayout->addWidget(yScaleLabel,0);
    yScaleLayout->addWidget(yScaleNum,1);

    zScaleLayout->addWidget(zScaleLabel,0);
    zScaleLayout->addWidget(zScaleNum,1);

    QWidget* scaleWidget = new QWidget();

    QVBoxLayout* scaleLayout = new QVBoxLayout();
    scaleLayout->addLayout(xScaleLayout);
    scaleLayout->addLayout(yScaleLayout);
    scaleLayout->addLayout(zScaleLayout);

    scaleWidget->setLayout(scaleLayout);

    textureBtn = new QPushButton("Set Texture");
    connect(textureBtn,SIGNAL(clicked()),this,SLOT(updateTexture()));

    lightBtn = new QPushButton("Set Light Proprieties");
    connect(lightBtn,SIGNAL(clicked()),this,SLOT(lightDialog()));

    objectPositionSettings->addTab(scaleWidget,QIcon("icons/scale.png"),"Scale");

    //initialize list to send to renderArea
    list = new QTreeWidget();
    list->setHeaderHidden(true);
    connect(list,SIGNAL(itemSelectionChanged()),this,SLOT(selectObject()));

    lightHandler=new LightHandler(list);
    timeline = new Timeline(&projectInfo,list);

    QGraphicsView *view = new QGraphicsView(timeline);
    view->setVerticalScrollBarPolicy ( Qt::ScrollBarAlwaysOff );

    modelHandler = new ModelHandler();
    renderArea = new RenderArea(parent,list,modelHandler,&projectInfo,timeline,xPosNum,yPosNum,zPosNum,xRotNum,yRotNum,zRotNum,wRotNum,xScaleNum,yScaleNum,zScaleNum);

    render->addWidget(renderArea);

    timelineLayout->addWidget(view);

    right->addWidget(list,5);
    right->addWidget(objectType);
    right->addWidget(objectModel);
    right->addWidget(objectName);
    right->addWidget(boneLengthLabel);
    right->addWidget(boneLengthSetting);
    right->addWidget(objectNameSetting);
    right->addWidget(textureBtn);
    right->addWidget(lightBtn);
    right->addWidget(objectPositionSettings);

    objectType->hide();
    objectModel->hide();
    boneLengthLabel->hide();
    boneLengthSetting->hide();
    objectNameSetting->hide();
    objectName->hide();
    textureBtn->hide();
    lightBtn->hide();
    objectPositionSettings->hide();

    left->addLayout(top,1);
    left->addLayout(render,8);
    left->addWidget(bottomWidget,1);

    windowLayout->addLayout(left,7);
    windowLayout->addLayout(right,3);

    windowWidget->setLayout(windowLayout);
    setCentralWidget(windowWidget);
    projectHandler=new ProjectHandler(&projectInfo,list,modelHandler,lightHandler,objectModel);
}

void MainWindow::saveProject(){
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save Project"),"", tr("Rig Animate Project (*.ra)"));
    projectHandler->saveProject(fileName);
}
void MainWindow::importProject(){
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Project"),"", tr("Rig Animate Project (*.ra)"));
    projectHandler->openProject(fileName);
    renderArea->repaint();
}
void MainWindow::addObject(){
    int indexToAdd = list->topLevelItemCount();
    ObjectItem* item = new ObjectItem();
    item->setText(0,QString("NewObject"));
    list->insertTopLevelItem(indexToAdd,item);
}
void MainWindow::removeObject(){
     if(list->currentIndex().row()>=0){
         if(!(list->currentItem()->parent())){
             ObjectItem* object = static_cast<ObjectItem*>(list->currentItem());
             if(object->data.type==2){
                 for(int j=object->childCount()-1; j>=0;j--){
                     BoneItem* bone = static_cast<BoneItem *>(object->child(j));
                     bone->deleteALlBones();
                 }
             }
             delete list->takeTopLevelItem(list->currentIndex().row());
             renderArea->repaint();
         } else {
             BoneItem* bone = static_cast<BoneItem *>(list->currentItem());
             bone->deleteALlBones();
             renderArea->repaint();
         }
     }
}
void MainWindow::toggleMode(){
    projectInfo.windowMode=!projectInfo.windowMode;
    QPushButton * button = static_cast<QPushButton *>(this->sender());
    if(projectInfo.windowMode){
        button->setIcon(QIcon("icons/toggle-anim.png"));
    } else {
        button->setIcon(QIcon("icons/toggle-rig.png"));
    }
    if(list->currentIndex().row()>=0){
        selectObject();
    }
    renderArea->repaint();
}
void MainWindow::addBone(){
    if(list->currentIndex().row()>=0 && projectInfo.windowMode){
        BoneItem* itemToAdd = new BoneItem();
        itemToAdd->setText(0,QString("NewBone"));
        if(!(list->currentItem()->parent())){
            ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
            if(item->data.type==2){
                list->currentItem()->addChild(itemToAdd);
            }
        } else {
            list->currentItem()->addChild(itemToAdd);
        }
        renderArea->repaint();
    }
}

void MainWindow::togglePlay(){
    if(!projectInfo.windowMode){
        projectInfo.isPlaying=!projectInfo.isPlaying;
        QPushButton * button = static_cast<QPushButton *>(this->sender());
        if(projectInfo.isPlaying){
            button->setIcon(QIcon("icons/pause.png"));
            renderArea->play();
        } else {
            button->setIcon(QIcon("icons/play.png"));
            renderArea->pause();
        }
    }
}

void MainWindow::addKeyframe(){
    if(list->currentIndex().row()>=0 && !projectInfo.windowMode){
        int alreadyExisted=-1;
        if(!(list->currentItem()->parent())){
            ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
            alreadyExisted = item->addKeyframe(projectInfo.currentFrame);
        } else {
            BoneItem* bone = static_cast<BoneItem *>(list->currentItem());
            alreadyExisted = bone->addKeyframe(projectInfo.currentFrame);
        }
        if(alreadyExisted==0){
            QGraphicsEllipseItem *keyframe = timeline->addEllipse(0,40,10,10,QPen(QColor(51,102,255),3));
            keyframe->setX(projectInfo.currentFrame);
            keyframe->setFlag(QGraphicsItem::ItemIsSelectable);
        }
    }
}

void MainWindow::removeKeyframe(){
    if(list->currentIndex().row()>=0 && !projectInfo.windowMode){
        QList<QGraphicsItem*> allGraphicsItems = timeline->selectedItems();
        if(!(list->currentItem()->parent())){
            ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
            for(int i = 0; i < allGraphicsItems.size(); i++)
            {
                QGraphicsItem *graphicItem = allGraphicsItems[i];
                if(graphicItem->type()==4){
                    item->removeKeyframe(graphicItem->x());
                    timeline->removeItem(graphicItem);
                    delete graphicItem;
                }
            }
        } else {
            BoneItem* bone = static_cast<BoneItem *>(list->currentItem());
            for(int i = 0; i < allGraphicsItems.size(); i++)
            {
                QGraphicsItem *graphicItem = allGraphicsItems[i];
                if(graphicItem->type()==4){
                    bone->removeKeyframe(graphicItem->x());
                    timeline->removeItem(graphicItem);
                    delete graphicItem;
                }
            }
        }
        renderArea->repaint();
    }
}

void MainWindow::updateObjectType(int type){
    if(list->currentIndex().row()>=0){
        ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
        if(item->data.type==type) return;
        int prevType = item->data.type;
        if(prevType==1){
            item->data.modelId=0;
        } else if(prevType==2){
            for(int j=item->childCount()-1; j>=0;j--){
                BoneItem* bone = static_cast<BoneItem *>(item->child(j));
                bone->deleteALlBones();
            }
        } else if(prevType==3){
            lightHandler->removeLight(item->data.lightID);
            item->data.lightID=-1;
        }
        item->data.type=type;
        if(type==1){
            list->currentItem()->setIcon(0,QIcon("icons/obj.png"));
        } else if(type==2){
            list->currentItem()->setIcon(0,QIcon("icons/rig.png"));
        } else if(type==3){
            list->currentItem()->setIcon(0,QIcon("icons/light.png"));
            item->data.lightID = lightHandler->addLight();
        } else {
            list->currentItem()->setIcon(0,QIcon());
        }
        renderArea->repaint();
        selectObject();
    }
}
void MainWindow::updateObjectModel(int index){
    if(list->currentIndex().row()>=0){
        if(!(list->currentItem()->parent())){
            ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
            item->data.modelId=index;
        } else {
            BoneItem* item = static_cast<BoneItem *>(list->currentItem());
            item->data.modelId=index;
        }
        if(index==1){
            QString fileName = QFileDialog::getOpenFileName(this,tr("Open OBJ file"), "", tr("OBJ Files (*.obj)"));
            if(fileName.isEmpty() || fileName.isNull()){
                objectModel->setCurrentIndex(0);
                return;
            }
            int model = modelHandler->loadModel(fileName.toStdString());
            std::string path = modelHandler->getPath(model);
            std::string base_filename = path.substr(path.find_last_of("/\\") + 1);
            std::string::size_type const p(base_filename.find_last_of('.'));
            std::string file_without_extension = base_filename.substr(0, p);
            objectModel->addItem(QString::fromStdString(file_without_extension));
            objectModel->setCurrentIndex(model+2);
            return;
        }
        renderArea->repaint();
    }
}
void MainWindow::openSettings(){
    QDialog *widget = new QDialog;
    QVBoxLayout *panel = new QVBoxLayout();

    QLabel *frameNumberLabel = new QLabel("Number of Frames");
    QSpinBox* frameNumberText = new QSpinBox;
    frameNumberText->setMaximum(60*500);
    frameNumberText->setValue(projectInfo.frameNumber);
    connect(frameNumberText, SIGNAL(valueChanged(int)), this, SLOT(updateFrameNumber(int)));

    QLabel *frameRateLabel = new QLabel("Frames per Second");
    QSpinBox* frameRateText = new QSpinBox;
    frameRateText->setRange(1,300);
    frameRateText->setValue(projectInfo.frameRate);
    connect(frameRateText, SIGNAL(valueChanged(int)), this, SLOT(updateFrameRate(int)));

    panel->addWidget(frameNumberLabel);
    panel->addWidget(frameNumberText);

    panel->addWidget(frameRateLabel);
    panel->addWidget(frameRateText);

    widget->setLayout(panel);
    widget->exec();
}
void MainWindow::updateFrameNumber(int newFrameNum){
    projectInfo.frameNumber=newFrameNum;
    timeline->setSceneRect(0,0,newFrameNum,100);
    for(int i = 0; i < list->topLevelItemCount(); i++){
        ObjectItem* item = static_cast<ObjectItem *>(list->topLevelItem(i));
        item->deleteExtraKeyframes(newFrameNum);
    }
    QList<QGraphicsItem*> allGraphicsItems = timeline->items();
    for(int i = 0; i < allGraphicsItems.size(); i++)
    {
        QGraphicsItem *graphicItem = allGraphicsItems[i];
        if(graphicItem->type()==4 && graphicItem->x()>newFrameNum){
            timeline->removeItem(graphicItem);
            delete graphicItem;
        }
    }
}
void MainWindow::updateFrameRate(int newFrameRate){
    projectInfo.frameRate=newFrameRate;
}
void MainWindow::selectObject()
{
    QList<QGraphicsItem*> allGraphicsItems = timeline->items();
    for(int i = 0; i < allGraphicsItems.size(); i++)
    {
        QGraphicsItem *graphicItem = allGraphicsItems[i];
        if(graphicItem->type()==4){
            timeline->removeItem(graphicItem);
            delete graphicItem;
        }
    }
    objectType->hide();
    objectModel->hide();
    objectNameSetting->hide();
    objectName->hide();
    textureBtn->hide();
    lightBtn->hide();
    objectPositionSettings->hide();
    boneLengthLabel->hide();
    boneLengthSetting->hide();
    if(list->currentIndex().row()>=0){
        if(!(list->currentItem()->parent())){
            objectType->show();
            ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
            setObjectSettings(item);
            return;
        }
        BoneItem* item = static_cast<BoneItem *>(list->currentItem());
        setBoneSettings(item);
        return;
    }
}
void MainWindow::setObjectSettings(ObjectItem * item){
    for(int i=0; i<(int)item->data.keyframes.size();i++){
        QGraphicsEllipseItem *keyframe = timeline->addEllipse(0,40,10,10,QPen(QColor(51,102,255),3));
        keyframe->setX(item->data.keyframes.at(i).frame);
        keyframe->setFlag(QGraphicsItem::ItemIsSelectable);
    }
    if(projectInfo.windowMode){
        objectNameSetting->setText(item->text(0));
        objectNameSetting->show();
    } else {
        objectName->setText(item->text(0));
        objectName->show();
    }
    int type = item->data.type;
    objectType->setCurrentIndex(type);
    if(type==1 && projectInfo.windowMode){
        objectModel->show();
        objectModel->setCurrentIndex(item->data.modelId);
        textureBtn->show();
    } else if(type==1 && !projectInfo.windowMode){
        objectPositionSettings->show();
        xPosNum->setValue(item->data.pos[0]);
        yPosNum->setValue(item->data.pos[1]);
        zPosNum->setValue(item->data.pos[2]);
        xRotNum->setValue(item->data.q->x);
        yRotNum->setValue(item->data.q->y);
        zRotNum->setValue(item->data.q->z);
        wRotNum->setValue(item->data.q->w);
        xScaleNum->setValue(item->data.scale[0]);
        yScaleNum->setValue(item->data.scale[1]);
        zScaleNum->setValue(item->data.scale[2]);
    } else if(type==2 && !projectInfo.windowMode){
        objectPositionSettings->show();
        xPosNum->setValue(item->data.pos[0]);
        yPosNum->setValue(item->data.pos[1]);
        zPosNum->setValue(item->data.pos[2]);
        xRotNum->setValue(item->data.q->x);
        yRotNum->setValue(item->data.q->y);
        zRotNum->setValue(item->data.q->z);
        wRotNum->setValue(item->data.q->w);
        xScaleNum->setValue(item->data.scale[0]);
        yScaleNum->setValue(item->data.scale[1]);
        zScaleNum->setValue(item->data.scale[2]);
    } else if(type==3 && !projectInfo.windowMode){
        objectPositionSettings->show();
        xPosNum->setValue(item->data.pos[0]);
        yPosNum->setValue(item->data.pos[1]);
        zPosNum->setValue(item->data.pos[2]);
        xRotNum->setValue(item->data.q->x);
        yRotNum->setValue(item->data.q->y);
        zRotNum->setValue(item->data.q->z);
        wRotNum->setValue(item->data.q->w);
        xScaleNum->setValue(item->data.scale[0]);
        yScaleNum->setValue(item->data.scale[1]);
        zScaleNum->setValue(item->data.scale[2]);
    } else if(type==3 && projectInfo.windowMode){
        lightBtn->show();
    }
}

void MainWindow::setBoneSettings(BoneItem * item){
    for(int i=0; i<(int)item->data.keyframes.size();i++){
        QGraphicsEllipseItem *keyframe = timeline->addEllipse(0,40,10,10,QPen(QColor(51,102,255),3));
        keyframe->setX(item->data.keyframes.at(i).frame);
        keyframe->setFlag(QGraphicsItem::ItemIsSelectable);
    }
    if(projectInfo.windowMode){
        objectNameSetting->setText(item->text(0));
        objectNameSetting->show();
        boneLengthLabel->show();
        boneLengthSetting->setValue(item->data.length);
        boneLengthSetting->show();
        objectModel->show();
        objectModel->setCurrentIndex(item->data.modelId);
        textureBtn->show();
        objectPositionSettings->show();
        xPosNum->setValue(item->data.localpos[0]);
        yPosNum->setValue(item->data.localpos[1]);
        zPosNum->setValue(item->data.localpos[2]);
        xRotNum->setValue(item->data.localq->x);
        yRotNum->setValue(item->data.localq->y);
        zRotNum->setValue(item->data.localq->z);
        wRotNum->setValue(item->data.localq->w);
        xScaleNum->setValue(item->data.localscale[0]);
        yScaleNum->setValue(item->data.localscale[1]);
        zScaleNum->setValue(item->data.localscale[2]);
    } else {
        objectName->setText(item->text(0));
        objectName->show();
        objectPositionSettings->show();
        xPosNum->setValue(item->data.pos[0]);
        yPosNum->setValue(item->data.pos[1]);
        zPosNum->setValue(item->data.pos[2]);
        xRotNum->setValue(item->data.q->x);
        yRotNum->setValue(item->data.q->y);
        zRotNum->setValue(item->data.q->z);
        wRotNum->setValue(item->data.q->w);
        xScaleNum->setValue(item->data.scale[0]);
        yScaleNum->setValue(item->data.scale[1]);
        zScaleNum->setValue(item->data.scale[2]);
    }
}

void MainWindow::updateName(QString text){
    if(list->currentColumn()>=0){
        if(!(list->currentItem()->parent())){
            ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
            item->setText(0,text);
        } else {
            BoneItem* item = static_cast<BoneItem *>(list->currentItem());
            item->setText(0,text);
        }
    }
}

void MainWindow::updateTexture(){
    if(list->currentColumn()>=0){
        QString path = QFileDialog::getOpenFileName(this,tr("Select Texture"), "", tr("Images (*.ppm *.png *.jpg)"));
        if(path.isEmpty() || path.isNull()){
            return;
        }
        QImage* a = new QImage(path);
        QImage image = a->rgbSwapped();
        if(!(list->currentItem()->parent())){
            ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
            item->data.texture=image;
            item->data.texturePath=path.toStdString();
        } else {
            BoneItem* item = static_cast<BoneItem *>(list->currentItem());
            item->data.texture=image;
            item->data.texturePath=path.toStdString();
        }
    }
}

void MainWindow::setBoneLength(double length){
    if(list->currentColumn()>=0){
        if(!(list->currentItem()->parent())){
            return;
        }
        BoneItem* item = static_cast<BoneItem *>(list->currentItem());
        item->data.length=length;
        renderArea->repaint();
    }
}

void MainWindow::updateObjectXPos(double pos){
    if(list->currentIndex().row()>=0){
        if(!(list->currentItem()->parent())){
            ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
            item->data.pos[0]=pos;
        } else {
            BoneItem* item = static_cast<BoneItem *>(list->currentItem());
            if(projectInfo.windowMode){
                item->data.localpos[0]=pos;
            } else {
                item->data.pos[0]=pos;
            }
        }
    }
    renderArea->repaint();
}
void MainWindow::updateObjectYPos(double pos){
    if(list->currentIndex().row()>=0){
        if(!(list->currentItem()->parent())){
            ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
            item->data.pos[1]=pos;
        } else {
            BoneItem* item = static_cast<BoneItem *>(list->currentItem());
            if(projectInfo.windowMode){
                item->data.localpos[1]=pos;
            } else {
                item->data.pos[1]=pos;
            }
        }
    }
    renderArea->repaint();
}
void MainWindow::updateObjectZPos(double pos){
    if(list->currentIndex().row()>=0){
        if(!(list->currentItem()->parent())){
            ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
            item->data.pos[2]=pos;
        } else {
            BoneItem* item = static_cast<BoneItem *>(list->currentItem());
            if(projectInfo.windowMode){
                item->data.localpos[2]=pos;
            } else {
                item->data.pos[2]=pos;
            }
        }
    }
    renderArea->repaint();
}
void MainWindow::updateObjectXRot(double pos){
    if(list->currentIndex().row()>=0){
        if(!(list->currentItem()->parent())){
            ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
            item->data.q->x=pos;
        } else {
            BoneItem* item = static_cast<BoneItem *>(list->currentItem());
            if(projectInfo.windowMode){
                item->data.localq->x=pos;
            } else {
                item->data.q->x=pos;
            }
        }
        updateRotationValues();
    }
    renderArea->repaint();
}
void MainWindow::updateObjectYRot(double pos){
    if(list->currentIndex().row()>=0){
        if(!(list->currentItem()->parent())){
            ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
            item->data.q->y=pos;
        } else {
            BoneItem* item = static_cast<BoneItem *>(list->currentItem());
            if(projectInfo.windowMode){
                item->data.localq->y=pos;
            } else {
                item->data.q->y=pos;
            }
        }
        updateRotationValues();
    }
    renderArea->repaint();
}
void MainWindow::updateObjectZRot(double pos){
    if(list->currentIndex().row()>=0){
        if(!(list->currentItem()->parent())){
            ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
            item->data.q->z=pos;
        } else {
            BoneItem* item = static_cast<BoneItem *>(list->currentItem());
            if(projectInfo.windowMode){
                item->data.localq->z=pos;
            } else {
                item->data.q->z=pos;
            }
        }
        updateRotationValues();
    }
    renderArea->repaint();
}
void MainWindow::updateObjectWRot(double pos){
    if(list->currentIndex().row()>=0){
        if(!(list->currentItem()->parent())){
            ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
            item->data.q->w=pos;
        } else {
            BoneItem* item = static_cast<BoneItem *>(list->currentItem());
            if(projectInfo.windowMode){
                item->data.localq->w=pos;
            } else {
                item->data.q->w=pos;
            }
        }
        updateRotationValues();
    }
    renderArea->repaint();
}
void MainWindow::updateRotationValues(){
    if(list->currentIndex().row()>=0){
        if(!(list->currentItem()->parent())){
            ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
            item->data.q->normalize();
            xRotNum->blockSignals(true);
            xRotNum->setValue(item->data.q->x);
            xRotNum->blockSignals(false);

            yRotNum->blockSignals(true);
            yRotNum->setValue(item->data.q->y);
            yRotNum->blockSignals(false);

            zRotNum->blockSignals(true);
            zRotNum->setValue(item->data.q->z);
            zRotNum->blockSignals(false);

            wRotNum->blockSignals(true);
            wRotNum->setValue(item->data.q->w);
            wRotNum->blockSignals(false);
        } else {
            BoneItem* item = static_cast<BoneItem *>(list->currentItem());
            if(projectInfo.windowMode){
                item->data.localq->normalize();
                xRotNum->blockSignals(true);
                xRotNum->setValue(item->data.localq->x);
                xRotNum->blockSignals(false);

                yRotNum->blockSignals(true);
                yRotNum->setValue(item->data.localq->y);
                yRotNum->blockSignals(false);

                zRotNum->blockSignals(true);
                zRotNum->setValue(item->data.localq->z);
                zRotNum->blockSignals(false);

                wRotNum->blockSignals(true);
                wRotNum->setValue(item->data.localq->w);
                wRotNum->blockSignals(false);
            } else {
                item->data.q->normalize();
                xRotNum->blockSignals(true);
                xRotNum->setValue(item->data.q->x);
                xRotNum->blockSignals(false);

                yRotNum->blockSignals(true);
                yRotNum->setValue(item->data.q->y);
                yRotNum->blockSignals(false);

                zRotNum->blockSignals(true);
                zRotNum->setValue(item->data.q->z);
                zRotNum->blockSignals(false);

                wRotNum->blockSignals(true);
                wRotNum->setValue(item->data.q->w);
                wRotNum->blockSignals(false);
            }
        }
    }
}
void MainWindow::updateObjectXScale(double pos){
    if(list->currentIndex().row()>=0){
        if(!(list->currentItem()->parent())){
            ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
            item->data.scale[0]=pos;
        } else {
            BoneItem* item = static_cast<BoneItem *>(list->currentItem());
            if(projectInfo.windowMode){
                item->data.localscale[0]=pos;
            } else {
                item->data.scale[0]=pos;
            }
        }
    }
    renderArea->repaint();
}
void MainWindow::updateObjectYScale(double pos){
    if(list->currentIndex().row()>=0){
        if(!(list->currentItem()->parent())){
            ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
            item->data.scale[1]=pos;
        } else {
            BoneItem* item = static_cast<BoneItem *>(list->currentItem());
            if(projectInfo.windowMode){
                item->data.localscale[1]=pos;
            } else {
                item->data.scale[1]=pos;
            }
        }
    }
    renderArea->repaint();
}
void MainWindow::updateObjectZScale(double pos){
    if(list->currentIndex().row()>=0){
        if(!(list->currentItem()->parent())){
            ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
            item->data.scale[2]=pos;
        } else {
            BoneItem* item = static_cast<BoneItem *>(list->currentItem());
            if(projectInfo.windowMode){
                item->data.localscale[2]=pos;
            } else {
                item->data.scale[2]=pos;
            }
        }
    }
    renderArea->repaint();
}

void MainWindow::lightDialog(){
    if(list->currentIndex().row()>=0){
        ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
        std::cout << item->data.lightID << std::endl;
        if(item->data.lightID>-1){
            QDialog *widget = new QDialog;
            QVBoxLayout *panel = new QVBoxLayout();

            QLabel *lalb1 = new QLabel("Light Ambient Red");
            QSlider* lasl1 = new QSlider(Qt::Horizontal);
            lasl1->setMinimum(0);
            lasl1->setMaximum(100);
            lasl1->setValue(lightHandler->lights.at(item->data.lightID).l[0][0]*100);
            connect(lasl1, SIGNAL(valueChanged(int)), this, SLOT(updateLightAmbientX(int)));

            QLabel *lalb2 = new QLabel("Light Ambient Green");
            QSlider* lasl2 = new QSlider(Qt::Horizontal);
            lasl2->setMinimum(0);
            lasl2->setMaximum(100);
            lasl2->setValue(lightHandler->lights.at(item->data.lightID).l[0][1]*100);
            connect(lasl2, SIGNAL(valueChanged(int)), this, SLOT(updateLightAmbientY(int)));

            QLabel *lalb3 = new QLabel("Light Ambient Blue");
            QSlider* lasl3 = new QSlider(Qt::Horizontal);
            lasl3->setMinimum(0);
            lasl3->setMaximum(100);
            lasl3->setValue(lightHandler->lights.at(item->data.lightID).l[0][2]*100);
            connect(lasl3, SIGNAL(valueChanged(int)), this, SLOT(updateLightAmbientZ(int)));


            QLabel *ldlb1 = new QLabel("Light Diffuse Red");
            QSlider* ldsl1 = new QSlider(Qt::Horizontal);
            ldsl1->setMinimum(0);
            ldsl1->setMaximum(100);
            ldsl1->setValue(lightHandler->lights.at(item->data.lightID).l[1][0]*100);
            connect(ldsl1, SIGNAL(valueChanged(int)), this, SLOT(updateLightDiffuseX(int)));

            QLabel *ldlb2 = new QLabel("Light Diffuse Green");
            QSlider* ldsl2 = new QSlider(Qt::Horizontal);
            ldsl2->setMinimum(0);
            ldsl2->setMaximum(100);
            ldsl2->setValue(lightHandler->lights.at(item->data.lightID).l[1][1]*100);
            connect(ldsl2, SIGNAL(valueChanged(int)), this, SLOT(updateLightDiffuseY(int)));

            QLabel *ldlb3 = new QLabel("Light Diffuse Blue");
            QSlider* ldsl3 = new QSlider(Qt::Horizontal);
            ldsl3->setMinimum(0);
            ldsl3->setMaximum(100);
            ldsl3->setValue(lightHandler->lights.at(item->data.lightID).l[1][2]*100);
            connect(ldsl3, SIGNAL(valueChanged(int)), this, SLOT(updateLightDiffuseZ(int)));


            QLabel *lslb1 = new QLabel("Light Specular Red");
            QSlider* lssl1 = new QSlider(Qt::Horizontal);
            lssl1->setMinimum(0);
            lssl1->setMaximum(100);
            lssl1->setValue(lightHandler->lights.at(item->data.lightID).l[2][0]*100);
            connect(lssl1, SIGNAL(valueChanged(int)), this, SLOT(updateLightSpecularX(int)));

            QLabel *lslb2 = new QLabel("Light Specular Green");
            QSlider* lssl2 = new QSlider(Qt::Horizontal);
            lssl2->setMinimum(0);
            lssl2->setMaximum(100);
            lssl2->setValue(lightHandler->lights.at(item->data.lightID).l[2][1]*100);
            connect(lssl2, SIGNAL(valueChanged(int)), this, SLOT(updateLightSpecularY(int)));

            QLabel *lslb3 = new QLabel("Light Specular Blue");
            QSlider* lssl3 = new QSlider(Qt::Horizontal);
            lssl3->setMinimum(0);
            lssl3->setMaximum(100);
            lssl3->setValue(lightHandler->lights.at(item->data.lightID).l[2][2]*100);
            connect(lssl3, SIGNAL(valueChanged(int)), this, SLOT(updateLightSpecularZ(int)));

            panel->addWidget(lalb1);
            panel->addWidget(lasl1);
            panel->addWidget(lalb2);
            panel->addWidget(lasl2);
            panel->addWidget(lalb3);
            panel->addWidget(lasl3);

            panel->addWidget(ldlb1);
            panel->addWidget(ldsl1);
            panel->addWidget(ldlb2);
            panel->addWidget(ldsl2);
            panel->addWidget(ldlb3);
            panel->addWidget(ldsl3);

            panel->addWidget(lslb1);
            panel->addWidget(lssl1);
            panel->addWidget(lslb2);
            panel->addWidget(lssl2);
            panel->addWidget(lslb3);
            panel->addWidget(lssl3);

            widget->setLayout(panel);
            widget->exec();
        }
    }
}

void MainWindow::updateLightAmbientX(int light){
    if(list->currentIndex().row()>=0){
        ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
        if(item->data.lightID>-1){
            lightHandler->updateLight(item->data.lightID,1,light);
        }
    }
    renderArea->repaint();
}

void MainWindow::updateLightAmbientY(int light){
    if(list->currentIndex().row()>=0){
        ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
        if(item->data.lightID>-1){
            lightHandler->updateLight(item->data.lightID,2,light);
        }
    }
    renderArea->repaint();
}

void MainWindow::updateLightAmbientZ(int light){
    if(list->currentIndex().row()>=0){
        ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
        if(item->data.lightID>-1){
            lightHandler->updateLight(item->data.lightID,3,light);
        }
    }
    renderArea->repaint();
}

void MainWindow::updateLightDiffuseX(int light){
    if(list->currentIndex().row()>=0){
        ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
        if(item->data.lightID>-1){
            lightHandler->updateLight(item->data.lightID,4,light);
        }
    }
    renderArea->repaint();
}

void MainWindow::updateLightDiffuseY(int light){
    if(list->currentIndex().row()>=0){
        ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
        if(item->data.lightID>-1){
            lightHandler->updateLight(item->data.lightID,5,light);
        }
    }
    renderArea->repaint();
}

void MainWindow::updateLightDiffuseZ(int light){
    if(list->currentIndex().row()>=0){
        ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
        if(item->data.lightID>-1){
            lightHandler->updateLight(item->data.lightID,6,light);
        }
    }
    renderArea->repaint();
}

void MainWindow::updateLightSpecularX(int light){
    if(list->currentIndex().row()>=0){
        ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
        if(item->data.lightID>-1){
            lightHandler->updateLight(item->data.lightID,7,light);
        }
    }
    renderArea->repaint();
}

void MainWindow::updateLightSpecularY(int light){
    if(list->currentIndex().row()>=0){
        ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
        if(item->data.lightID>-1){
            lightHandler->updateLight(item->data.lightID,8,light);
        }
    }
    renderArea->repaint();
}

void MainWindow::updateLightSpecularZ(int light){
    if(list->currentIndex().row()>=0){
        ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
        if(item->data.lightID>-1){
            lightHandler->updateLight(item->data.lightID,9,light);
        }
    }
    renderArea->repaint();
}




MainWindow::~MainWindow()
{
}

