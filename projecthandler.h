#ifndef PROJECTHANDLER_H
#define PROJECTHANDLER_H

#include <QString>
#include <QFile>
#include <QTextStream>
#include <projectinfo.h>
#include <modelhandler.h>
#include <lighthandler.h>
#include <keyframe.h>
#include <boneitem.h>
#include <string>
#include <fstream>
#include <QComboBox>
#include <QTreeWidget>

class ProjectHandler
{
public:
    ProjectHandler(projectInfoStruct* pi, QTreeWidget* ls, ModelHandler* mh, LightHandler* lh, QComboBox* cb);
    void saveProject(QString path);
    void openProject(QString path);
    void printRig(BoneItem* b, QTextStream* outStream);
    projectInfoStruct* projectInfo;
    QTreeWidget *list;
    ModelHandler* modelHandler;
    LightHandler* lightHandler;
    QComboBox *objectModel;
    ObjectItem *lastInsertedObject;
    BoneItem *lastInsertedBone;
    QTreeWidgetItem *lastInsertedObjectItem;
    QTreeWidgetItem *lastInsertedBoneItem;
    bool isLastObject;
    bool isNextChild;
    bool isNextParent;
};

#endif // PROJECTHANDLER_H
