#ifndef LIGHTHANDLER_H
#define LIGHTHANDLER_H

#include <vector>
#include <QTreeWidget>
#include <objectItem.h>
#include <GL/glu.h>

typedef struct lightStruct{
    GLfloat l[3][3];
}lightStruct;


class LightHandler
{
public:
    LightHandler(QTreeWidget* ls);
    int addLight();
    int addProjectLight(GLfloat val1, GLfloat val2, GLfloat val3, GLfloat val4, GLfloat val5, GLfloat val6, GLfloat val7, GLfloat val8, GLfloat val9);
    void removeLight(int id);
    void updateLight(int id, int prop, float value);
    std::vector<lightStruct> lights;
    QTreeWidget *list;
};

#endif // LIGHTHANDLER_H
