#ifndef MODELHANDLER_H
#define MODELHANDLER_H

#include <vector>
#include <string>
#include <fstream>
#include <GL/glu.h>

typedef struct modelStruct {
    std::string path;
    std::vector<std::vector<double>> vertices;
    std::vector<std::vector<double>> normals;
    std::vector<std::vector<double>> texture;
    std::vector<std::vector<int>> triangleVertices;
    std::vector<std::vector<int>> triangleTexture;
    std::vector<std::vector<int>> triangleNormals;
    bool hasNormals;
    bool hasTexture;
} modelStruct;

class ModelHandler
{
public:
    ModelHandler();
    int loadModel(std::string path);
    void drawModel(int id);
    std::string getPath(int id);

    std::vector<modelStruct> loadedModels;
};

#endif // MODELHANDLER_H
