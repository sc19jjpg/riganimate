#include "projecthandler.h"

ProjectHandler::ProjectHandler(projectInfoStruct* pi, QTreeWidget* ls, ModelHandler* mh, LightHandler* lh, QComboBox* cb)
{
    projectInfo=pi;
    list=ls;
    modelHandler=mh;
    lightHandler=lh;
    objectModel=cb;
}

void ProjectHandler::saveProject(QString path){
    QFile f( path );
    f.open( QIODevice::WriteOnly);
    QTextStream outStream(&f);

    outStream << "pc " << projectInfo->currentFrame << "\n";
    outStream << "pf " << projectInfo->frameNumber << "\n";
    outStream << "ps " << projectInfo->frameRate << "\n";
    outStream << "pw " << projectInfo->windowMode << "\n";
    outStream << "pp " << projectInfo->isPlaying << "\n";

    for(int i=0;i<(int)modelHandler->loadedModels.size();i++){
        outStream << "m " << QString::fromStdString(modelHandler->loadedModels.at(i).path) << "\n";
    }

    for(int i=0;i<(int)lightHandler->lights.size();i++){
        lightStruct l=lightHandler->lights.at(i);
        outStream << "l " << l.l[0][0] << " " << l.l[0][1] << " " << l.l[0][2] << " " << l.l[1][0] << " " << l.l[1][1] << " " << l.l[1][2] << " " << l.l[2][0] << " " << l.l[2][1] << " " << l.l[2][2] << "\n";
    }

    for(int i=0;i<list->topLevelItemCount();i++){
        ObjectItem* so = static_cast<ObjectItem *>(list->topLevelItem(i));
        outStream << "o " << so->data.type << " " << so->text(0) << " " << so->data.pos[0] << " " << so->data.pos[1] << " " << so->data.pos[2] << " " << so->data.q->x << " " << so->data.q->y << " " << so->data.q->z << " " << so->data.q->w << " " << so->data.scale[0] << " " << so->data.scale[1] << " " << so->data.scale[2] << " " << so->data.modelId << " " << so->data.lightID << " " << QString::fromStdString(so->data.texturePath) << " " << "\n";

        for(int j=0;j<(int)so->data.keyframes.size();j++){
            KeyframeStruct k = so->data.keyframes.at(j);
            outStream << "k " << k.frame << " " << k.pos[0] << " " << k.pos[1] << " " << k.pos[2] << " " << k.q->x << " " << k.q->y << " " << k.q->z << " " << k.q->w << " " << k.scale[0] << " " << k.scale[1] << " " << k.scale[2]  << "\n";
        }

        if(so->data.type==2){
            for(int j=0; j<list->topLevelItem(i)->childCount();j++){
                BoneItem* bone = static_cast<BoneItem *>(list->topLevelItem(i)->child(j));
                printRig(bone, &outStream);
            }
        }
    }
    f.close();
}
void ProjectHandler::printRig(BoneItem* b, QTextStream* outStream){
    *outStream << "ba " << b->text(0) << " " << b->data.pos[0] << " " << b->data.pos[1] << " " << b->data.pos[2] << " " << b->data.q->x << " " << b->data.q->y << " " << b->data.q->z << " " << b->data.q->w << " " << b->data.scale[0] << " " << b->data.scale[1] << " " << b->data.scale[2] << " " << b->data.localpos[0] << " " << b->data.localpos[1] << " " << b->data.localpos[2] << " " << b->data.localq->x << " " << b->data.localq->y << " " << b->data.localq->z << " " << b->data.localq->w << " " << b->data.localscale[0] << " " << b->data.localscale[1] << " " << b->data.localscale[2] << " " << b->data.modelId << " " << b->data.length << " " << QString::fromStdString(b->data.texturePath) << " " << "\n";

    for(int j=0;j<(int)b->data.keyframes.size();j++){
        KeyframeStruct k = b->data.keyframes.at(j);
        *outStream << "k " << k.frame << " " << k.pos[0] << " " << k.pos[1] << " " << k.pos[2] << " " << k.q->x << " " << k.q->y << " " << k.q->z << " " << k.q->w << " " << k.scale[0] << " " << k.scale[1] << " " << k.scale[2]  << "\n";
    }
    if(b->childCount()>0){
        *outStream << "bc " << "\n";
        for(int i=0;i<b->childCount();i++){
            BoneItem* newbone = static_cast<BoneItem *>(b->child(i));
            printRig(newbone,outStream);
        }
        *outStream << "bb "  << "\n";
    }
}
void ProjectHandler::openProject(QString path){
    std::string myText;

    std::ifstream MyReadFile(path.toStdString());
    if(!MyReadFile){
        return;
    }

    while (getline (MyReadFile, myText)) {
        if(myText[0]=='p'){
            if(myText[1]=='c'){
                std::sscanf(myText.c_str(),"pc %d",&projectInfo->currentFrame);
            } else if(myText[1]=='f'){
                std::sscanf(myText.c_str(),"pf %d",&projectInfo->frameNumber);
            } else if(myText[1]=='s'){
                std::sscanf(myText.c_str(),"ps %d",&projectInfo->frameRate);
            } else if(myText[1]=='w'){
                std::sscanf(myText.c_str(),"pw %d",&projectInfo->windowMode);
            } else if(myText[1]=='p'){
                std::sscanf(myText.c_str(),"pp %d",&projectInfo->isPlaying);
            }
        } else if(myText[0]=='m'){
            char modelPath[100];
            std::sscanf(myText.c_str(),"m %s",modelPath);
            std::string path(modelPath);
            modelHandler->loadModel(path);
            std::string base_filename = path.substr(path.find_last_of("/\\") + 1);
            std::string::size_type const p(base_filename.find_last_of('.'));
            std::string file_without_extension = base_filename.substr(0, p);
            objectModel->addItem(QString::fromStdString(file_without_extension));
        } else if(myText[0]=='l'){
            GLfloat l[3][3];
            std::sscanf(myText.c_str(),"l %f %f %f %f %f %f %f %f %f",&l[0][0],&l[0][1],&l[0][2],&l[1][0],&l[1][1],&l[1][2],&l[2][0],&l[2][1],&l[2][2]);
            lightHandler->addProjectLight(l[0][0],l[0][1],l[0][2],l[1][0],l[1][1],l[1][2],l[2][0],l[2][1],l[2][2]);
        } else if(myText[0]=='o'){
            isLastObject=true;
            char texturePath[100];
            char name[100];
            int indexToAdd = list->topLevelItemCount();
            sceneObjectStruct object;
            object.q=new Quaternion();

            std::sscanf(myText.c_str(),"o %d %s %lf %lf %lf %f %f %f %f %lf %lf %lf %d %d %s",&object.type,name,&object.pos[0],&object.pos[1],&object.pos[2],&object.q->x,&object.q->y,&object.q->z,&object.q->w,&object.scale[0],&object.scale[1],&object.scale[2],&object.modelId,&object.lightID,texturePath);
            ObjectItem* item = new ObjectItem(object);
            QImage* a = new QImage(texturePath);
            QImage image = a->rgbSwapped();
            item->data.texture=image;
            item->data.texturePath=std::string(texturePath);

            item->setText(0,QString(name));
            list->insertTopLevelItem(indexToAdd,item);

            lastInsertedObjectItem=list->topLevelItem(indexToAdd);
            if(item->data.type==1){
                lastInsertedObjectItem->setIcon(0,QIcon("icons/obj.png"));
            } else if(item->data.type==2){
                lastInsertedObjectItem->setIcon(0,QIcon("icons/rig.png"));
            } else if(item->data.type==3){
                lastInsertedObjectItem->setIcon(0,QIcon("icons/light.png"));
            } else {
                lastInsertedObjectItem->setIcon(0,QIcon());
            }
        } else if(myText[0]=='b'){
            if(myText[1]=='a'){
                char name[100];
                char texturePath[100];
                sceneBoneStruct bone;
                bone.q=new Quaternion();
                bone.localq= new Quaternion();

                std::sscanf(myText.c_str(),"ba %s %lf %lf %lf %f %f %f %f %lf %lf %lf %lf %lf %lf %f %f %f %f %lf %lf %lf %d %lf %s",name,&bone.pos[0],&bone.pos[1],&bone.pos[2],&bone.q->x,&bone.q->y,&bone.q->z,&bone.q->w,&bone.scale[0],&bone.scale[1],&bone.scale[2],&bone.localpos[0],&bone.localpos[1],&bone.localpos[2],&bone.localq->x,&bone.localq->y,&bone.localq->z,&bone.localq->w,&bone.localscale[0],&bone.localscale[1],&bone.localscale[2],&bone.modelId,&bone.length,texturePath);

                BoneItem* item = new BoneItem(bone);
                QImage* a = new QImage(texturePath);
                QImage image = a->rgbSwapped();
                item->data.texture=image;
                item->data.texturePath=std::string(texturePath);

                item->setText(0,QString(name));

                if(isLastObject){        
                    lastInsertedObjectItem->addChild(item);
                    lastInsertedBoneItem=lastInsertedObjectItem->child(lastInsertedObjectItem->childCount()-1);
                } else {
                    if(isNextChild){
                        isNextChild=false;
                        lastInsertedBoneItem->addChild(item);
                        lastInsertedBoneItem=lastInsertedBoneItem->child(lastInsertedBoneItem->childCount()-1);
                    } else if(isNextParent){
                        isNextParent=false;
                        lastInsertedBoneItem->parent()->parent()->addChild(item);
                        lastInsertedBoneItem=lastInsertedBoneItem->parent()->parent()->child(lastInsertedBoneItem->parent()->parent()->childCount()-1);
                    } else {
                        lastInsertedBoneItem->parent()->addChild(item);
                        lastInsertedBoneItem=lastInsertedBoneItem->parent()->child(lastInsertedBoneItem->parent()->childCount()-1);
                    }
                }
                isLastObject=false;
            } else if(myText[1]=='c'){
                isNextChild=true;
            } else if(myText[1]=='b'){
                isNextParent=true;
            }
        } else if(myText[0]=='k'){
            if(isLastObject){
                KeyframeStruct k;
                k.q=new Quaternion();
                ObjectItem* item = static_cast<ObjectItem *>(lastInsertedObjectItem);
                std::sscanf(myText.c_str(),"k %d %lf %lf %lf %f %f %f %f %lf %lf %lf %lf",&k.frame,&k.pos[0],&k.pos[1],&k.pos[2],&k.q->x,&k.q->y,&k.q->z,&k.q->w,&k.scale[0],&k.scale[1],&k.scale[2]);
                item->addProjectKeyframe(k);
            } else {
                KeyframeStruct k;
                k.q=new Quaternion();
                BoneItem* item = static_cast<BoneItem *>(lastInsertedBoneItem);
                std::sscanf(myText.c_str(),"k %d %lf %lf %lf %f %f %f %f %lf %lf %lf %lf",&k.frame,&k.pos[0],&k.pos[1],&k.pos[2],&k.q->x,&k.q->y,&k.q->z,&k.q->w,&k.scale[0],&k.scale[1],&k.scale[2]);
                item->addProjectKeyframe(k);
            }
        }
    }
}
