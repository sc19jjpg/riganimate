#include "objectItem.h"

ObjectItem::ObjectItem()
{
    data.type=0;
    data.modelId=0;
    data.pos[0]=0;
    data.pos[1]=0;
    data.pos[2]=0;
    data.scale[0]=1;
    data.scale[1]=1;
    data.scale[2]=1;
    data.q=new Quaternion();
    data.lightID=-1;
    QTreeWidgetItem();
}
ObjectItem::ObjectItem(sceneObjectStruct object)
{
   data=object;
   QTreeWidgetItem();
}
int ObjectItem::addKeyframe(int frame){
    for(int i=0;i<(int)data.keyframes.size();i++){
        if(data.keyframes[i].frame==frame){
            data.keyframes[i].pos[0]=data.pos[0];
            data.keyframes[i].pos[1]=data.pos[1];
            data.keyframes[i].pos[2]=data.pos[2];
            data.keyframes[i].q->x=data.q->x;
            data.keyframes[i].q->y=data.q->y;
            data.keyframes[i].q->z=data.q->z;
            data.keyframes[i].q->w=data.q->w;
            data.keyframes[i].scale[0]=data.scale[0];
            data.keyframes[i].scale[1]=data.scale[1];
            data.keyframes[i].scale[2]=data.scale[2];
            return 1;
        }
    }
    KeyframeStruct k;
    k.q=new Quaternion();
    k.frame=frame;
    k.pos[0]=data.pos[0];
    k.pos[1]=data.pos[1];
    k.pos[2]=data.pos[2];
    k.q->x=data.q->x;
    k.q->y=data.q->y;
    k.q->z=data.q->z;
    k.q->w=data.q->w;
    k.scale[0]=data.scale[0];
    k.scale[1]=data.scale[1];
    k.scale[2]=data.scale[2];
    data.keyframes.push_back(k);
    return 0;
}
void ObjectItem::addProjectKeyframe(KeyframeStruct k){
    KeyframeStruct ks;
    ks.q=new Quaternion();
    ks.frame=k.frame;
    ks.pos[0]=k.pos[0];
    ks.pos[1]=k.pos[1];
    ks.pos[2]=k.pos[2];
    ks.q->x=k.q->x;
    ks.q->y=k.q->y;
    ks.q->z=k.q->z;
    ks.q->w=k.q->w;
    ks.scale[0]=k.scale[0];
    ks.scale[1]=k.scale[1];
    ks.scale[2]=k.scale[2];
    data.keyframes.push_back(ks);
}

void ObjectItem::removeKeyframe(int frame){
    for(int i=0;i<(int)data.keyframes.size();i++){
        if(data.keyframes.at(i).frame==frame){
            data.keyframes.erase(data.keyframes.begin()+i);
            return;
        }
    }
}

void ObjectItem::deleteExtraKeyframes(int newFrameNum){
    for(int i=data.keyframes.size()-1;i>=0;i--){
        if(data.keyframes.at(i).frame>newFrameNum){
            data.keyframes.erase(data.keyframes.begin()+i);
        }
    }
}

float* ObjectItem::generateMatrix(){
    float* matrix=new float[16];
    matrix[0]=data.scale[0]*(data.q->w*data.q->w+data.q->x*data.q->x-data.q->y*data.q->y-data.q->z*data.q->z);
    matrix[4]=data.scale[1]*(2*data.q->x*data.q->y-2*data.q->w*data.q->z);
    matrix[8]=data.scale[2]*(2*data.q->x*data.q->z+2*data.q->w*data.q->y);

    matrix[1]=data.scale[0]*(2*data.q->x*data.q->y+2*data.q->w*data.q->z);
    matrix[5]=data.scale[1]*(data.q->w*data.q->w-data.q->x*data.q->x+data.q->y*data.q->y-data.q->z*data.q->z);
    matrix[9]=data.scale[2]*(2*data.q->y*data.q->z-2*data.q->w*data.q->x);

    matrix[2]=data.scale[0]*(2*data.q->x*data.q->z-2*data.q->w*data.q->y);
    matrix[6]=data.scale[1]*(2*data.q->y*data.q->z+2*data.q->w*data.q->x);
    matrix[10]=data.scale[2]*(data.q->w*data.q->w-data.q->x*data.q->x-data.q->y*data.q->y+data.q->z*data.q->z);

    matrix[3]=0;
    matrix[7]=0;
    matrix[11]=0;

    matrix[12]=data.pos[0];
    matrix[13]=data.pos[1];
    matrix[14]=data.pos[2];
    matrix[15]=1;

    return matrix;
}
