#ifndef QUATERNION_H
#define QUATERNION_H

#include <math.h>

class Quaternion
{
public:
    Quaternion();
    Quaternion(float x, float y, float z, float w);
    float length();
    void normalize();
    void multiply(Quaternion* q);
    float x;
    float y;
    float z;
    float w;
};

#endif // QUATERNION_H
