#include "modelhandler.h"

ModelHandler::ModelHandler()
{

}

int ModelHandler::loadModel(std::string path)
{
    modelStruct model;
    model.path=path;
    model.hasNormals=false;
    model.hasTexture=false;

    std::string myText;

    std::ifstream MyReadFile(path);
    if(!MyReadFile){
        return -1;
    }

    while (getline (MyReadFile, myText)) {
        if(myText[0]=='v'){
            std::vector<double> numbers(3);
            if(myText[1]==' '){
                std::sscanf(myText.c_str(),"v %lf %lf %lf",&numbers[0],&numbers[1],&numbers[2]);
                model.vertices.push_back(numbers);
            } else if(myText[1]=='t'){
                model.hasTexture=true;
                std::sscanf(myText.c_str(),"vt %lf %lf",&numbers[0],&numbers[1]);
                model.texture.push_back(numbers);
            } else if(myText[1]=='n'){
                std::sscanf(myText.c_str(),"vn %lf %lf %lf",&numbers[0],&numbers[1],&numbers[2]);
                model.hasNormals=true;
                model.normals.push_back(numbers);
            }
        } else if(myText[0]=='f'){
            if(model.hasNormals && model.hasTexture){
                std::vector<int> vertices(3);
                std::vector<int> texture(3);
                std::vector<int> normals(3);
                std::sscanf(myText.c_str(),"f %d/%d/%d %d/%d/%d %d/%d/%d",&vertices[0],&texture[0],&normals[0],&vertices[1],&texture[1],&normals[1],&vertices[2],&texture[2],&normals[2]);
                model.triangleVertices.push_back(vertices);
                model.triangleTexture.push_back(texture);
                model.triangleNormals.push_back(normals);
            } else if(model.hasNormals && !model.hasTexture){
                std::vector<int> vertices(3);
                std::vector<int> normals(3);
                std::sscanf(myText.c_str(),"f %d//%d %d//%d %d//%d",&vertices[0],&normals[0],&vertices[1],&normals[1],&vertices[2],&normals[2]);
                model.triangleVertices.push_back(vertices);
                model.triangleNormals.push_back(normals);
            } else if(!model.hasNormals && model.hasTexture){
                std::vector<int> vertices(3);
                std::vector<int> texture(3);
                std::sscanf(myText.c_str(),"f %d/%d/ %d/%d/ %d/%d/",&vertices[0],&texture[0],&vertices[1],&texture[1],&vertices[2],&texture[2]);
                model.triangleVertices.push_back(vertices);
                model.triangleTexture.push_back(texture);
            } else {
                std::vector<int> numbers(3);
                std::sscanf(myText.c_str(),"f %d %d %d",&numbers[0],&numbers[1],&numbers[2]);
                model.triangleVertices.push_back(numbers);
            }
        }
    }
    loadedModels.push_back(model);

    MyReadFile.close();
    return loadedModels.size()-1;
}

std::string ModelHandler::getPath(int id){
    return loadedModels.at(id).path;
}

void ModelHandler::drawModel(int id){
    if((int)loadedModels.size()<=id){
        //model id out of bounds, skip drawing it.
        return;
    }
    modelStruct model = loadedModels.at(id);
    glBegin(GL_TRIANGLES);
    for(int i=0;i<(int)model.triangleVertices.size();i++){
        if(model.hasTexture){
            glTexCoord2f(model.texture.at((model.triangleTexture.at(i).at(0)-1)).at(0), model.texture.at((model.triangleTexture.at(i).at(0)-1)).at(1));
        }
        if(model.hasNormals){
            glNormal3f(model.normals.at((model.triangleNormals.at(i).at(0)-1)).at(0), model.normals.at((model.triangleNormals.at(i).at(0)-1)).at(1), model.normals.at((model.triangleNormals.at(i).at(0)-1)).at(2));
        }
        glVertex3f(model.vertices.at((model.triangleVertices.at(i).at(0)-1)).at(0), model.vertices.at((model.triangleVertices.at(i).at(0)-1)).at(1), model.vertices.at((model.triangleVertices.at(i).at(0)-1)).at(2));
        if(model.hasTexture){
            glTexCoord2f(model.texture.at((model.triangleTexture.at(i).at(1)-1)).at(0), model.texture.at((model.triangleTexture.at(i).at(1)-1)).at(1));
        }
        if(model.hasNormals){
            glNormal3f(model.normals.at((model.triangleNormals.at(i).at(1)-1)).at(0), model.normals.at((model.triangleNormals.at(i).at(1)-1)).at(1), model.normals.at((model.triangleNormals.at(i).at(1)-1)).at(2));
        }
        glVertex3f(model.vertices.at((model.triangleVertices.at(i).at(1)-1)).at(0), model.vertices.at((model.triangleVertices.at(i).at(1)-1)).at(1), model.vertices.at((model.triangleVertices.at(i).at(1)-1)).at(2));
        if(model.hasTexture){
            glTexCoord2f(model.texture.at((model.triangleTexture.at(i).at(2)-1)).at(0), model.texture.at((model.triangleTexture.at(i).at(2)-1)).at(1));
        }
        if(model.hasNormals){
            glNormal3f(model.normals.at((model.triangleNormals.at(i).at(2)-1)).at(0), model.normals.at((model.triangleNormals.at(i).at(2)-1)).at(1), model.normals.at((model.triangleNormals.at(i).at(2)-1)).at(2));
        }
        glVertex3f(model.vertices.at((model.triangleVertices.at(i).at(2)-1)).at(0), model.vertices.at((model.triangleVertices.at(i).at(2)-1)).at(1), model.vertices.at((model.triangleVertices.at(i).at(2)-1)).at(2));
    }
    glEnd();
}
