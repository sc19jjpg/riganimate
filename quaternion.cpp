#include "quaternion.h"

Quaternion::Quaternion()
{
    x=0;
    y=0;
    z=0;
    w=1;
}

Quaternion::Quaternion(float x, float y, float z, float w){
    this->x=x;
    this->y=y;
    this->z=z;
    this->w=w;
}

float Quaternion::length(){
    return (float) sqrt(x*x+y*y+z*z+w*w);
}

void Quaternion::normalize(){
    float len = length();
    x/=len;
    y/=len;
    z/=len;
    w/=len;
}

void Quaternion::multiply(Quaternion* q){
    float q_x = w*q->x + x*q->w - y*q->z + z*q->y;
    float q_y = w*q->y + x*q->z + y*q->w - z*q->x;
    float q_z = w*q->z - x*q->y + y*q->x + z*q->w;
    float q_w = w*q->w - x*q->x - y*q->y - z*q->z;

    x=q_x;
    y=q_y;
    z=q_z;
    w=q_w;
    normalize();
}
