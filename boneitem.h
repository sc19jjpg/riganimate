#ifndef BONEITEM_H
#define BONEITEM_H

#include <QTreeWidgetItem>
#include <quaternion.h>
#include <keyframe.h>
#include <QImage>
#include <vector>

typedef struct sceneBoneStruct {
    int modelId;
    double localpos[3];
    double localscale[3];
    double length;
    double pos[3];
    double scale[3];
    Quaternion* q;
    Quaternion* localq;
    std::vector<KeyframeStruct> keyframes;
    std::string texturePath;
    QImage texture;
} sceneBoneStruct;

class BoneItem : public QTreeWidgetItem
{
public:
    BoneItem();
    BoneItem(sceneBoneStruct bone);
    int addKeyframe(int frame);
    void addProjectKeyframe(KeyframeStruct k);
    void removeKeyframe(int frame);
    void deleteALlBones();
    sceneBoneStruct data;
    float* generateMatrix();
    float* generateLocalMatrix();
};

#endif // BONEITEM_H
