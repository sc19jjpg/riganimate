#include "boneitem.h"

BoneItem::BoneItem()
{
    data.modelId=0;
    data.localpos[0]=0;
    data.localpos[1]=0;
    data.localpos[2]=0;
    data.localscale[0]=1;
    data.localscale[1]=1;
    data.localscale[2]=1;
    data.length=30;
    data.pos[0]=0;
    data.pos[1]=0;
    data.pos[2]=0;
    data.scale[0]=1;
    data.scale[1]=1;
    data.scale[2]=1;
    data.q=new Quaternion();
    data.localq=new Quaternion();
    QTreeWidgetItem();
}

BoneItem::BoneItem(sceneBoneStruct bone){
    data=bone;
    QTreeWidgetItem();
}

void BoneItem::deleteALlBones(){
    for(int i=this->childCount()-1;i>=0;i--){
        BoneItem* newbone = static_cast<BoneItem *>(this->child(i));
        newbone->deleteALlBones();
    }
    delete this;
}

int BoneItem::addKeyframe(int frame){
    for(int i=0;i<(int)data.keyframes.size();i++){
        if(data.keyframes[i].frame==frame){
            data.keyframes[i].pos[0]=data.pos[0];
            data.keyframes[i].pos[1]=data.pos[1];
            data.keyframes[i].pos[2]=data.pos[2];
            data.keyframes[i].q->x=data.q->x;
            data.keyframes[i].q->y=data.q->y;
            data.keyframes[i].q->z=data.q->z;
            data.keyframes[i].q->w=data.q->w;
            data.keyframes[i].scale[0]=data.scale[0];
            data.keyframes[i].scale[1]=data.scale[1];
            data.keyframes[i].scale[2]=data.scale[2];
            return 1;
        }
    }
    KeyframeStruct k;
    k.q=new Quaternion();
    k.frame=frame;
    k.pos[0]=data.pos[0];
    k.pos[1]=data.pos[1];
    k.pos[2]=data.pos[2];
    k.q->x=data.q->x;
    k.q->y=data.q->y;
    k.q->z=data.q->z;
    k.q->w=data.q->w;
    k.scale[0]=data.scale[0];
    k.scale[1]=data.scale[1];
    k.scale[2]=data.scale[2];
    data.keyframes.push_back(k);
    return 0;
}

void BoneItem::addProjectKeyframe(KeyframeStruct k){
    KeyframeStruct ks;
    ks.q=new Quaternion();
    ks.frame=k.frame;
    ks.pos[0]=k.pos[0];
    ks.pos[1]=k.pos[1];
    ks.pos[2]=k.pos[2];
    ks.q->x=k.q->x;
    ks.q->y=k.q->y;
    ks.q->z=k.q->z;
    ks.q->w=k.q->w;
    ks.scale[0]=k.scale[0];
    ks.scale[1]=k.scale[1];
    ks.scale[2]=k.scale[2];
    data.keyframes.push_back(ks);
}

void BoneItem::removeKeyframe(int frame){
    for(int i=0;i<(int)data.keyframes.size();i++){
        if(data.keyframes.at(i).frame==frame){
            data.keyframes.erase(data.keyframes.begin()+i);
            return;
        }
    }
}

float* BoneItem::generateMatrix(){
    float* matrix=new float[16];
    matrix[0]=data.scale[0]*(data.q->w*data.q->w+data.q->x*data.q->x-data.q->y*data.q->y-data.q->z*data.q->z);
    matrix[4]=data.scale[1]*(2*data.q->x*data.q->y-2*data.q->w*data.q->z);
    matrix[8]=data.scale[2]*(2*data.q->x*data.q->z+2*data.q->w*data.q->y);

    matrix[1]=data.scale[0]*(2*data.q->x*data.q->y+2*data.q->w*data.q->z);
    matrix[5]=data.scale[1]*(data.q->w*data.q->w-data.q->x*data.q->x+data.q->y*data.q->y-data.q->z*data.q->z);
    matrix[9]=data.scale[2]*(2*data.q->y*data.q->z-2*data.q->w*data.q->x);

    matrix[2]=data.scale[0]*(2*data.q->x*data.q->z-2*data.q->w*data.q->y);
    matrix[6]=data.scale[1]*(2*data.q->y*data.q->z+2*data.q->w*data.q->x);
    matrix[10]=data.scale[2]*(data.q->w*data.q->w-data.q->x*data.q->x-data.q->y*data.q->y+data.q->z*data.q->z);

    matrix[3]=0;
    matrix[7]=0;
    matrix[11]=0;

    matrix[12]=data.pos[0];
    matrix[13]=data.pos[1];
    matrix[14]=data.pos[2];
    matrix[15]=1;
    return matrix;
}

float* BoneItem::generateLocalMatrix(){
    float* matrix=new float[16];
    matrix[0]=data.localscale[0]*(data.localq->w*data.localq->w+data.localq->x*data.localq->x-data.localq->y*data.localq->y-data.localq->z*data.localq->z);
    matrix[4]=data.localscale[1]*(2*data.localq->x*data.localq->y-2*data.localq->w*data.localq->z);
    matrix[8]=data.localscale[2]*(2*data.localq->x*data.localq->z+2*data.localq->w*data.localq->y);

    matrix[1]=data.localscale[0]*(2*data.localq->x*data.localq->y+2*data.localq->w*data.localq->z);
    matrix[5]=data.localscale[1]*(data.localq->w*data.localq->w-data.localq->x*data.localq->x+data.localq->y*data.localq->y-data.localq->z*data.localq->z);
    matrix[9]=data.localscale[2]*(2*data.localq->y*data.localq->z-2*data.localq->w*data.localq->x);

    matrix[2]=data.localscale[0]*(2*data.localq->x*data.localq->z-2*data.localq->w*data.localq->y);
    matrix[6]=data.localscale[1]*(2*data.localq->y*data.localq->z+2*data.localq->w*data.localq->x);
    matrix[10]=data.localscale[2]*(data.localq->w*data.localq->w-data.localq->x*data.localq->x-data.localq->y*data.localq->y+data.localq->z*data.localq->z);

    matrix[3]=0;
    matrix[7]=0;
    matrix[11]=0;

    matrix[12]=data.localpos[0];
    matrix[13]=data.localpos[1];
    matrix[14]=data.localpos[2];
    matrix[15]=1;
    return matrix;
}
