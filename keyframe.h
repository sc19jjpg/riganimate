#ifndef KEYFRAME_H
#define KEYFRAME_H

#include <quaternion.h>

typedef struct KeyframeStruct{
    int frame;
    double pos[3];
    double scale[3];
    Quaternion* q;
}KeyframeStruct;

#endif // KEYFRAME_H
