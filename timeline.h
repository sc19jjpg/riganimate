#ifndef TIMELINE_H
#define TIMELINE_H

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsView>
#include <QGraphicsLineItem>
#include <QDoubleSpinBox>
#include <QTreeWidget>
#include <objectItem.h>
#include <boneitem.h>
#include <projectinfo.h>

class Timeline: public QGraphicsScene
{
    Q_OBJECT
public:
    Timeline(projectInfoStruct* pi, QTreeWidget* ls);
    void setFrame(int frame);
    void setBoneFrame(BoneItem* so, int frame);
    void setObjectPose(ObjectItem* so, KeyframeStruct keyframe);
    void setBonePose(BoneItem* so, KeyframeStruct keyframe);
    void setObjectInterpolatedPose(ObjectItem* so, KeyframeStruct previousKeyframe, KeyframeStruct nextKeyframe, float interpolationFactor);
    void setBoneInterpolatedPose(BoneItem* so, KeyframeStruct previousKeyframe, KeyframeStruct nextKeyframe, float interpolationFactor);
    projectInfoStruct* projectInfo;
    QGraphicsLineItem *cursor;
    QDoubleSpinBox* xPosNum;
    QDoubleSpinBox* yPosNum;
    QDoubleSpinBox* zPosNum;
    QDoubleSpinBox* xRotNum;
    QDoubleSpinBox* yRotNum;
    QDoubleSpinBox* zRotNum;
    QDoubleSpinBox* wRotNum;
    QDoubleSpinBox* xScaleNum;
    QDoubleSpinBox* yScaleNum;
    QDoubleSpinBox* zScaleNum;
    QTreeWidget *list;
public slots:
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *e);
};

#endif // TIMELINE_H
