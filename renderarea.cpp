#include "renderarea.h"

RenderArea::RenderArea(QWidget *parent, QTreeWidget* ls,ModelHandler* mh, projectInfoStruct* pi, Timeline* tl, QDoubleSpinBox* xp, QDoubleSpinBox* yp, QDoubleSpinBox* zp, QDoubleSpinBox* xr, QDoubleSpinBox* yr, QDoubleSpinBox* zr, QDoubleSpinBox* wr, QDoubleSpinBox* xs, QDoubleSpinBox* ys, QDoubleSpinBox* zs) : QGLWidget(parent)
{
    list=ls;
    modelHandler=mh;
    projectInfo=pi;
    timer = new QTimer;
    elapsed = new QElapsedTimer;
    timeline=tl;
    connect(timer, SIGNAL(timeout()), this, SLOT(timerUpdate()));
    xPosNum=xp;
    yPosNum=yp;
    zPosNum=zp;
    xRotNum=xr;
    yRotNum=yr;
    zRotNum=zr;
    wRotNum=wr;
    xScaleNum=xs;
    yScaleNum=ys;
    zScaleNum=zs;
}

void RenderArea::initializeGL(){
    glClearColor(0.3, 0.3, 0.3, 0.0);
    glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );

    glEnable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_NORMALIZE);
    glShadeModel(GL_SMOOTH);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
}

void RenderArea::resizeGL(int w, int h){
    width=w;
    height=h;
    radius = std::min(width/2,height/2);
    glViewport(0, 0, w, h);
    glEnable(GL_DEPTH_TEST);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-100.0, 100.0, -100.0, 100.0, -1000.0, 1000.0);
    gluLookAt(0,0,50,0,0,-1,0,1,0);
}
void RenderArea::play(){
    timer->start();
    elapsed->restart();
}
void RenderArea::pause(){
    timer->stop();
    if(list->currentIndex().row()>=0){
        if(!(list->currentItem()->parent())){
            ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
            xPosNum->setValue(item->data.pos[0]);
            yPosNum->setValue(item->data.pos[1]);
            zPosNum->setValue(item->data.pos[2]);
            xRotNum->setValue(item->data.q->x);
            yRotNum->setValue(item->data.q->y);
            zRotNum->setValue(item->data.q->z);
            wRotNum->setValue(item->data.q->w);
            xScaleNum->setValue(item->data.scale[0]);
            yScaleNum->setValue(item->data.scale[1]);
            zScaleNum->setValue(item->data.scale[2]);
        } else {
            BoneItem* item = static_cast<BoneItem *>(list->currentItem());
            xPosNum->setValue(item->data.pos[0]);
            yPosNum->setValue(item->data.pos[1]);
            zPosNum->setValue(item->data.pos[2]);
            xRotNum->setValue(item->data.q->x);
            yRotNum->setValue(item->data.q->y);
            zRotNum->setValue(item->data.q->z);
            wRotNum->setValue(item->data.q->w);
            xScaleNum->setValue(item->data.scale[0]);
            yScaleNum->setValue(item->data.scale[1]);
            zScaleNum->setValue(item->data.scale[2]);
        }
    }
}
void RenderArea::timerUpdate(){
    if(elapsed->elapsed()>=1000/projectInfo->frameRate){
        elapsed->restart();
        if(projectInfo->currentFrame<projectInfo->frameNumber){
            projectInfo->currentFrame++;
        }else{
            projectInfo->currentFrame=0;
        }
        timeline->setFrame(projectInfo->currentFrame);
        repaint();
    }
}
void RenderArea::mousePressEvent(QMouseEvent *e){
    if(list->currentIndex().row()>=0 && !projectInfo->isPlaying){
        if(!(list->currentItem()->parent())){
            ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
            press_x=item->data.q->x;
            press_y=item->data.q->y;
            press_z=item->data.q->z;
            press_w=item->data.q->w;
        } else {
            BoneItem* item = static_cast<BoneItem *>(list->currentItem());
            if(projectInfo->windowMode){
                press_x=item->data.localq->x;
                press_y=item->data.localq->y;
                press_z=item->data.localq->z;
                press_w=item->data.localq->w;
            } else {
                press_x=item->data.q->x;
                press_y=item->data.q->y;
                press_z=item->data.q->z;
                press_w=item->data.q->w;
            }
        }
        ndc_x = (float) (e->x() - width/2)/radius;
        ndc_y = (float) (height/2 - e->y())/radius;
        float squared = ndc_x*ndc_x + ndc_y*ndc_y;
        ndc_z=0;
        if(squared>1){
            float s=1/sqrt(squared);
            ndc_x*=s;
            ndc_y*=s;
        } else {
            ndc_z=sqrt(1-squared);
        }
    }
}

void RenderArea::mouseMoveEvent(QMouseEvent* e){
    float currentndc_x,currentndc_y,currentndc_z;
    currentndc_x = (float) (e->x() - width/2)/radius;
    currentndc_y = (float) (height/2 - e->y())/radius;
    float squared = currentndc_x*currentndc_x + currentndc_y*currentndc_y;
    currentndc_z=0;
    if(squared>1){
        float s=1/sqrt(squared);
        currentndc_x*=s;
        currentndc_y*=s;
    } else {
        currentndc_z=sqrt(1-squared);
    }
    float angle = acos(ndc_x*currentndc_x + ndc_y*currentndc_y + ndc_z*currentndc_z);
    //cross product to get the axis
    float axis_x = ndc_y*currentndc_z - ndc_z*currentndc_y;
    float axis_y = ndc_z*currentndc_x - ndc_x*currentndc_z;
    float axis_z = ndc_x*currentndc_y - ndc_y*currentndc_x;
    float s = sin(angle/2);
    Quaternion* tempQ = new Quaternion(axis_x*s,axis_y*s,axis_z*s,cos(angle/2));
    Quaternion* oldQ= new Quaternion(press_x,press_y,press_z,press_w);
    oldQ->multiply(tempQ);
    if(list->currentIndex().row()>=0 && !projectInfo->isPlaying){
        if(!(list->currentItem()->parent())){
            ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
            item->data.q->x=oldQ->x;
            item->data.q->y=oldQ->y;
            item->data.q->z=oldQ->z;
            item->data.q->w=oldQ->w;
        } else {
            BoneItem* item = static_cast<BoneItem *>(list->currentItem());
            if(projectInfo->windowMode){
                item->data.localq->x=oldQ->x;
                item->data.localq->y=oldQ->y;
                item->data.localq->z=oldQ->z;
                item->data.localq->w=oldQ->w;
            } else {
                item->data.q->x=oldQ->x;
                item->data.q->y=oldQ->y;
                item->data.q->z=oldQ->z;
                item->data.q->w=oldQ->w;
            }
        }
    }
    repaint();
}

void RenderArea::mouseReleaseEvent(QMouseEvent* e){
    if(list->currentIndex().row()>=0){
        if(!(list->currentItem()->parent())){
            ObjectItem* item = static_cast<ObjectItem *>(list->currentItem());
            xPosNum->setValue(item->data.pos[0]);
            yPosNum->setValue(item->data.pos[1]);
            zPosNum->setValue(item->data.pos[2]);
            xRotNum->setValue(item->data.q->x);
            yRotNum->setValue(item->data.q->y);
            zRotNum->setValue(item->data.q->z);
            wRotNum->setValue(item->data.q->w);
            xScaleNum->setValue(item->data.scale[0]);
            yScaleNum->setValue(item->data.scale[1]);
            zScaleNum->setValue(item->data.scale[2]);
        } else {
            BoneItem* item = static_cast<BoneItem *>(list->currentItem());
            if(projectInfo->windowMode){
                xPosNum->setValue(item->data.localpos[0]);
                yPosNum->setValue(item->data.localpos[1]);
                zPosNum->setValue(item->data.localpos[2]);
                xRotNum->setValue(item->data.localq->x);
                yRotNum->setValue(item->data.localq->y);
                zRotNum->setValue(item->data.localq->z);
                wRotNum->setValue(item->data.localq->w);
                xScaleNum->setValue(item->data.localscale[0]);
                yScaleNum->setValue(item->data.localscale[1]);
                zScaleNum->setValue(item->data.localscale[2]);
            } else {
                xPosNum->setValue(item->data.pos[0]);
                yPosNum->setValue(item->data.pos[1]);
                zPosNum->setValue(item->data.pos[2]);
                xRotNum->setValue(item->data.q->x);
                yRotNum->setValue(item->data.q->y);
                zRotNum->setValue(item->data.q->z);
                wRotNum->setValue(item->data.q->w);
                xScaleNum->setValue(item->data.scale[0]);
                yScaleNum->setValue(item->data.scale[1]);
                zScaleNum->setValue(item->data.scale[2]);
            }
        }
    }
}

void RenderArea::renderObjects(){
    for(int i=0;i<list->topLevelItemCount();i++){
        ObjectItem* so = static_cast<ObjectItem *>(list->topLevelItem(i));
        if(so->data.type==0) continue; //type not defined yet
        if(so->data.type==1){ //simple object
            if(so->data.modelId==0 || so->data.modelId==1) continue; //model not set
            glPushMatrix();
            GLfloat* matrix = so->generateMatrix();
            glMultMatrixf(matrix);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, so->data.texture.width(), so->data.texture.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, so->data.texture.bits());
            modelHandler->drawModel(so->data.modelId-2);
            glPopMatrix();
            continue;
        }
        if(so->data.type==2){
            glPushMatrix();
            GLfloat* matrix = so->generateMatrix();
            glMultMatrixf(matrix);
            for(int j=0; j<list->topLevelItem(i)->childCount();j++){
                BoneItem* bone = static_cast<BoneItem *>(list->topLevelItem(i)->child(j));
                renderRig(bone);
            }
            glPopMatrix();
            continue;
        }
        if(so->data.type==3){
            glPushMatrix();
            GLfloat* matrix = so->generateMatrix();
            glMultMatrixf(matrix);
            GLfloat light_pos[] = {0., 0., 0., 1.};
            glLightfv(GL_LIGHT0+so->data.lightID, GL_POSITION, light_pos);
            glPopMatrix();
            continue;
        }
    }
}

void RenderArea::renderRig(BoneItem *bone){
    glPushMatrix();
    GLfloat* matrix = bone->generateMatrix();
    glMultMatrixf(matrix);
    if(projectInfo->windowMode){
        glBegin(GL_QUADS);
            glVertex3d(-10,0,10);
            glVertex3d(10,0,10);
            glVertex3d(10,0,-10);
            glVertex3d(-10,0,-10);
        glEnd();
        glBegin(GL_TRIANGLES);
            glVertex3d(-10,0,10);
            glVertex3d(10,0,10);
            glVertex3d(0,bone->data.length,0);

            glVertex3d(10,0,10);
            glVertex3d(10,0,-10);
            glVertex3d(0,bone->data.length,0);

            glVertex3d(10,0,-10);
            glVertex3d(-10,0,-10);
            glVertex3d(0,bone->data.length,0);

            glVertex3d(-10,0,-10);
            glVertex3d(-10,0,10);
            glVertex3d(0,bone->data.length,0);
        glEnd();
    }
    glPushMatrix();
    GLfloat* localmatrix = bone->generateLocalMatrix();
    glMultMatrixf(localmatrix);
        if(bone->data.modelId>1){
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bone->data.texture.width(), bone->data.texture.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, bone->data.texture.bits());
            modelHandler->drawModel(bone->data.modelId-2);
        }
    glPopMatrix();
    glTranslatef(0,bone->data.length,0);
    for(int i=0;i<bone->childCount();i++){
        BoneItem* newbone = static_cast<BoneItem *>(bone->child(i));
        renderRig(newbone);
    }
    glPopMatrix();
}

void RenderArea::paintGL(){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    renderObjects();

    glFlush();
}
