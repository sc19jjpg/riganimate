#ifndef PROJECTINFO_H
#define PROJECTINFO_H

typedef struct projectInfoStruct {
    bool isPlaying;
    int currentFrame;
    int frameRate;
    int frameNumber;
    bool windowMode; //false=animation mode, true=rigging mode
} projectInfoStruct;

#endif // PROJECTINFO_H
